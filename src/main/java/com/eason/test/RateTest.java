package com.eason.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.eason.model.Rate;
import com.eason.model.RateWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;


public class RateTest {

	public static void main(String[] args) throws IOException {
		
//		ObjectMapper mapper = new ObjectMapper();
//        try {
//            String result = mapper.readValue(new URL("https://tw.rter.info/capi.php"), String.class);
//            System.out.println(result);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        
//        URL url = new URL("https://tw.rter.info/capi.php");
//        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
//        conn.setRequestMethod("GET");
//        Map<String, List<String>> header = conn.getHeaderFields();
//        int responseCode = conn.getResponseCode();
//        System.out.println("Headers : "+header);
//        System.out.println("Response Code "+responseCode);
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
//        String line = "";
//        while((line = bufferedReader.readLine()) !=null) {
//        	System.out.println(line);
//        }
		
		ObjectMapper mapper = new ObjectMapper();
		ResourceBundle bundle = ResourceBundle.getBundle("info");
		String rateUrl = bundle.getString("exchange.rate.url");
		
		String rateTime = "";
		Double twdJpyRate = 0.0;
		
		
		
			URL url = new URL(rateUrl);
			
			RateWrapper rateWrapper = mapper.readValue(url, RateWrapper.class);
			
			
			Double eurjpy = 0.0;
			Double eurtwd = 0.0;
			
			Rate rate = rateWrapper.getRates();
			
			eurjpy = rate.getJpy();
			eurtwd = rate.getTwd();
			
//			Double targetBf = (eurtwd/eurjpy)*1.107;
			Double targetBf = (eurtwd/eurjpy)*1.168;
			
			
			BigDecimal bd= new BigDecimal(targetBf);   
			bd=bd.setScale(3, BigDecimal.ROUND_HALF_UP);// 小數後面四位, 四捨五入   
			twdJpyRate = bd.doubleValue();
			System.out.println(">>>" + twdJpyRate);
		
	}

}
