package com.eason.model;


import com.fasterxml.jackson.annotation.JsonProperty;
public class Rate{
	
//	@JsonProperty("UTC")
//	private String utc;
	@JsonProperty("TWD")
	private Double twd;
	@JsonProperty("JPY")
	private Double jpy;
	public Double getTwd() {
		return twd;
	}
	public void setTwd(Double twd) {
		this.twd = twd;
	}
	public Double getJpy() {
		return jpy;
	}
	public void setJpy(Double jpy) {
		this.jpy = jpy;
	}
	
	
}
