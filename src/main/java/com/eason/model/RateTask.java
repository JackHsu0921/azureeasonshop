package com.eason.model;

import java.io.IOException;
import java.util.TimerTask;

import javax.servlet.ServletContext;

import com.eason.controller.ContextListener;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class RateTask extends TimerTask{
	
	static ServletContext context;
	
	
	public static ServletContext getContext() {
		return context;
	}


	public static void setContext(ServletContext context) {
		RateTask.context = context;
	}


	@Override
	public void run() {
		Double twdJpyRate = 0.309;
		try {
			twdJpyRate = ContextListener.getTWDJPYRate();
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		context.setAttribute("TWDJPYRate", twdJpyRate.toString());
	}
	
}
