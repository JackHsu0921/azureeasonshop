package com.eason.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.eason.model.Rate;
import com.eason.model.RateTask;
import com.eason.model.RateWrapper;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ContextListener implements ServletContextListener{
	
	ResourceBundle bundle = ResourceBundle.getBundle("info");
	String rateUrl = bundle.getString("exchange.rate.url");

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		 
		Double twdJpyRate = 0.309;
		try {
			twdJpyRate = getTWDJPYRate();
		} catch (IOException e) {
			e.printStackTrace();
		}
		sce.getServletContext().setAttribute("TWDJPYRate", twdJpyRate.toString());
		
		Timer timer = new Timer();
		RateTask.setContext(sce.getServletContext());
		RateTask rateTask = new RateTask();
		
		timer.scheduleAtFixedRate(rateTask, 10000, 86400000);
	}
	
	public static Double getTWDJPYRate() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ResourceBundle bundle = ResourceBundle.getBundle("info");
		String rateUrl = bundle.getString("exchange.rate.url");
		
		String rateTime = "";
		Double twdJpyRate = 0.0;
		
		
		
			URL url = new URL(rateUrl);
			
			RateWrapper rateWrapper = mapper.readValue(url, RateWrapper.class);
			
			
			Double eurjpy = 0.0;
			Double eurtwd = 0.0;
			
			Rate rate = rateWrapper.getRates();
			
			eurjpy = rate.getJpy();
			eurtwd = rate.getTwd();
			
			Double targetBf = (eurtwd/eurjpy)*1.168;
			
			BigDecimal bd= new BigDecimal(targetBf);    
			bd=bd.setScale(3, BigDecimal.ROUND_HALF_UP);// 小數後面四位, 四捨五入   
			twdJpyRate = bd.doubleValue();
			System.out.println(">>>>>>" + twdJpyRate);
		
		
		return twdJpyRate;
		
	}

	public static void main(String[] args) {
		Double rate = 0.309;
		try {
			rate = getTWDJPYRate();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
