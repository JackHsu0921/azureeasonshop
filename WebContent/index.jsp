<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<%
	String twdjpyRate = "";
	twdjpyRate = (String)request.getServletContext().getAttribute("TWDJPYRate");
	if(twdjpyRate == null || twdjpyRate.length() == 0 ){
		twdjpyRate = "0.276";
	}
%>
<!DOCTYPE>
<html>
<!--     <head th:insert="indexFragments/indexHeader :: indexHeader(~{::title},_)"> -->
    <head>
			<%@include file="includ/header.jsp" %>
  	</head>	
		
	<body style="font-family:Microsoft JhengHei;">
	
	<%@include file="includ/navbar.jsp" %>
	
	





	<!--幻燈片====================================================================-->
	<div id="carouselExampleIndicators" class="carousel slide mb-5" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img class="d-block w-100" src="img/carousel_as.jpg" alt="First slide">
<!-- 				<div class="carousel-caption d-none d-md-block"> -->
<!-- 					<h1>Build apps that move the world</h1> -->
<!-- 					<a class="btn btn-danger" href="">Get a free API test key</a> -->
<!-- 				</div> -->
			</div>
			
			<div class="carousel-item">
				<img class="d-block w-100" src="img/carousel_bs.jpg" alt="Second slide">
<!-- 				<div class="carousel-caption d-none d-md-block"> -->
<!-- 					<h1>Build apps that move the world</h1> -->
<!-- 					<a class="btn btn-danger" href="">Get a free API test key</a> -->
<!-- 				</div> -->
			</div>
			
			<div class="carousel-item">
				<img class="d-block w-100" src="img/carousel_c.jpg" alt="Third slide">
<!-- 				<div class="carousel-caption d-none d-md-block"> -->
<!-- 					<h1>Build apps that move the world</h1> -->
<!-- 					<a class="btn btn-danger" href="">Get a free API test key</a> -->
<!-- 				</div> -->
			</div>
		</div>
		
		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	

	<!--購買流程-->	
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-9">
				<div class="py-2">	
					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card">
						  <h2>簡單五步驟，讓您完成購買!</h2>
						  <div class="w3-large mb-3">
						  	  <font>1. 請先加入LINE好友之後，在網頁上選擇欲購買商品的日本網站。</font><br>
							  <font>2. 商品確認後用填寫訂單表格，並確定網站、數量、番號、尺寸、顏色、日幣價格。</font><br>
							  <font>3. 報價完成後，匯入款項，完成第一階段付款(下訂等廠商出貨)。</font><br>
							  <font>4. 國際運費報價，匯入款項，完成第二階段付款(等待商品到您手中)。</font><br>
							  <font>5. 完成購買。</font><br>
							  <a  href="https://line.me/R/ti/p/%40cfd3755c" target="_blank"><img class="w3-card" height="36" border="0" alt="加入好友" src="https://scdn.line-apps.com/n/line_add_friends/btn/zh-Hant.png"></a>
						  </div>
					</div>
					
					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card w3-large">
						    <div>			  
							  	<b class="w3-text-blue">第一階段付費:</b>		  
							  	<p>商品總價 = (稅後商品價格 + 日本國內運費、免運 0 運費) X 當日匯率 + 每件20台幣(最高只收100台幣)</p>
						    </div>
						     <div >			  
							  	<b class="w3-text-blue">第二階段付費:(請參考<a href="expense.jsp" class="w3-text-red" target="_blank">運費價格表</a>)</b>
			  					<p >使用日本郵政寄送總價 = 國際運費 X 當日匯率 (直寄到府免台灣運費)</p>
						    </div>   
					</div>
					
				</div>
			</div>
			<div class="col-xs-12 col-sm-3">
				<div class="col-xs-12 col-sm-12">
					<h3>本日匯率</h3>
				</div>
				<div class="col-xs-12 col-sm-12">
					<ul class="list-group w3-large">
					   <li   class="list-group-item">日幣 : <span id="jypRate"><%=twdjpyRate %></span></li>
 					<!--<li   class="list-group-item">日幣 : <span id="jypRate">0.286</span></li>-->
					</ul>
					<div class="text-center">
						<button onclick="document.getElementById('CountModal').style.display='block'" class="w3-btn w3-card w3-round w3-purple w3-border text-center mt-3 ">點擊價格試算</button>	
					</div>
					
					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card">
						<div class="d-flex flex-column">
						  <div class="p-2">
						  		<a href="https://docs.google.com/forms/d/e/1FAIpQLSfbcuiMIeNYWzRB_fzfwrjozuy9AUgrLCpXv6L67W3Xk13X5w/viewform" target="_blank" class="w3-btn w3-card w3-orange btn-lg btn-block" style="text-decoration:none;"><i class="far fa-hand-pointer mr-3"></i>立即下單</a>
						  </div>
						  <div class="p-2">
						  		<a href="https://docs.google.com/forms/d/e/1FAIpQLSeueIK2R4S7dpxId7hQGFpr7TIGyrnCAzsiaj_U7RR-4HC4Wg/viewform" target="_blank" class="w3-btn w3-card w3-orange btn-lg btn-block" style="text-decoration:none;"><i class="fab fa-amazon-pay mr-3"></i>第一次繳費</a>
						  </div>
						  <div class="p-2">
						  		<a href="https://docs.google.com/forms/d/e/1FAIpQLSetYhg94ECnvSmmPsOXUu7LVuQ9X8e35sBahykRw-gCFMCrOg/viewform" target="_blank" class="w3-btn w3-card w3-orange btn-lg btn-block" style="text-decoration:none;"><i class="fas fa-dolly-flatbed mr-3"></i>第二次繳費</a>
						  </div>
						  <div class="p-2">
						  		<b class="w3-text-blue">繳費方式可選擇:</b>	
						  		<ul>
<!-- 								  <li >信用卡繳費(手續費2.75%)</li> -->
								  <li >ATM/WebATM(免手續費)</li>
								  <li >超商代碼繳款(手續費30台幣)</li>					  
								</ul>
						  </div>
						</div>
							
							
							
					</div>
										
				</div>	 			
			</div>
			
<!-- 			<div class="col-xs-12 col-sm-12 "> -->
<!-- 					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card w3-large"> -->
<!-- 						    <div>			   -->
<!-- 							  	<b class="w3-text-blue">第一階段付費:</b>		   -->
<!-- 							  	<p>商品總價 = (稅後商品價格 + 日本國內運費、免運 0 運費) X 當日匯率 + 每件20台幣(最高只收100台幣)</p> -->
<!-- 						    </div> -->
<!-- 						     <div >			   -->
<!-- 							  	<b class="w3-text-blue">第二階段付費:(請參考<a href="expense.jsp" class="w3-text-red" target="_blank">運費價格表</a>)</b> -->
<!-- 			  					<p >使用日本郵政寄送總價 = 國際運費 X 當日匯率 (直寄到府免台灣運費)</p> -->
<!-- 							  	<p >使用集貨空運寄送總價 = 每1KG 300台幣 直寄到府免台灣運費 </p> -->
<!-- 							  	<p >使用集貨海運寄送總價 = 每1KG 150台幣 直寄到府免台灣運費 </p> -->
<!-- 						    </div>    -->
<!-- 					</div> -->
<!-- 			</div>	 		 -->
		</div>
	</div>



	<!-- 價格試算 Model	 -->
	<div id="CountModal" class="w3-modal">
		<div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
			<header class="w3-container w3-indigo">
				<span onclick="document.getElementById('CountModal').style.display='none'"
				class="w3-button w3-display-topright">&times;</span>
				<h2>價格試算</h2>
			</header>
			<div>			  
			  <div class="w3-pale-green px-3 py-2">			  
			  	<h4>第一階段付費:</h4>		  
			  	<p >商品總價 = (稅後商品價格 + 日本國內運費) X 當日匯率 + 每件20台幣(最高只收100台幣)</p>
<!-- 			  	<p >商品總報價(台幣) = (商品價格 + 商品價格10% + 日本國內運費 + 日本轉帳費) X 本日匯率</p> -->
			  </div>			  			  		    
			</div>

			<form class="w3-container">
				
				<label class="w3-text-blue"><b>商品價格總和(日幣)</b></label>
				<input class="w3-input w3-border" type="number" id="productPrice" onchange="firstPrice()" required>
				
				<label class="w3-text-blue"><b>一件商品20台幣(最高收取100台幣)</b></label>				
				<input class="w3-input w3-border" value="20" id="serviceFee" onchange="firstPrice()" type="text" required>				

				<label class="w3-text-blue"><b>日本國內運費總合(日幣)</b></label>
				<input class="w3-input w3-border" type="number" id="japFare" onchange="firstPrice()" value="0" required>


<!-- 				<label class="w3-text-blue"><b>日本轉帳費(日幣)</b></label> -->
<!-- 				<input class="w3-input w3-border" value="400" id="jpaTransfer" type="text" readonly> -->

				<label class="w3-text-blue"><b>當日即時匯率</b></label>
				<input class="w3-input w3-border" value="0.309" id="rate" type="text" readonly>													 
			</form> 

			<div class="w3-container w3-border-top w3-padding-16 w3-light-grey mt-3">
				<p>第一階段試算結果(台幣): <span id="firstConut" class="w3-large w3-text-red"></span>
			</div>
			<div class="w3-amber px-3 py-2">
			  	<h4>第二階段付費:</h4>
			  	<p >使用日本郵政寄送總價 = 國際運費 X 當日匯率 (直寄到府免台灣運費)</p>
			</div>
			<form class="w3-container">
				
				<label class="w3-text-blue"><b>國際運費(請參考<a href="expense.jsp" class="w3-text-red" target="_blank">運費價格表</a>)</b></label>
				<input class="w3-input w3-border" type="number" id="InternalPrice" onchange="secondPrice()" value="0" required>
				<label class="w3-text-blue"><b>當日即時匯率</b></label>
				<input class="w3-input w3-border" value="0.309" id="rate2" type="text" readonly>					 
				<!-- <label class="w3-text-blue">
					<b>集貨運費 (與國際運費<font class="w3-text-red">擇一</font>填寫) </b>
				</label>
				<input class="w3-input w3-border" type="number" id="twFare" onchange="secondPrice()" value="0" required> -->

			</form>
			<div class="w3-container w3-border-top w3-padding-16 w3-light-grey mt-3">
				<p>第二階段試算結果(台幣): <span id="secondCount" class="w3-large w3-text-red"></span>
				<p class="w3-right">總計(台幣): <span id="totalCount" class="w3-xlarge w3-text-red"></span>
			</div>			
		</div>
	</div>
	








	<!--相關網站====================================================================-->
	<div class="container-fluid mt-5 py-5" style="background-image: url('img/background.jpg')">
		<div class="row">
		
	<div class="container" >
		<div class="row">
			<div class="col-xs-12 col-sm-12 w3-bottombar w3-border-red mb-3 w3-wide">
				<h2 style="font-family:Microsoft JhengHei;text-shadow:1px 1px 0 #444" >相關網站</h2>
			</div>			
			<div class="col-xs-12 col-sm-3">
				<div class="py-2">
					<div class="w3-card">
						<header class="w3-container w3-blue w3-center">
						  <h4 style="font-family:Microsoft JhengHei;text-shadow:1px 1px 0 #444">綜合購物類</h4>
						</header>
						<div class="w3-container w3-light-gray w3-large">
						  	<ul>
							    <li><a href="https://www.amazon.co.jp/"  target="_blank">日本AMAZON</a></li>
							    <li><a href="https://www.rakuten.co.jp/"  target="_blank">日本樂天市場</a></li>
							    <li><a href="https://auctions.yahoo.co.jp/"  target="_blank">日本Yahoo拍賣</a></li>
							    <li><a href="https://www.suruga-ya.jp/"  target="_blank">駿河屋</a></li>
							    <li><a href="https://hands.net/"  target="_blank">Tokyu Hands</a></li>						
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-3">
				<div class="py-2">
					<div class="w3-card">
						<header class="w3-container w3-blue w3-center">
						  <h4 style="font-family:Microsoft JhengHei;text-shadow:1px 1px 0 #444">電器商品類</h4>
						</header>
						<div class="w3-container w3-light-gray w3-large">
						  <ul>						  
							  <li><a href="https://www.biccamera.com/bc/main/"  target="_blank">Big Camera</a></li>
							  <li><a href="https://online.nojima.co.jp/"  target="_blank">NOJIMA</a></li>
							  <li><a href="https://www.yamada-denkiweb.com/"  target="_blank">YAMADA電器</a></li>
							  <li><a href="https://www.yodobashi.com/"  target="_blank">Yodobashi電器</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-3">
				<div class="py-2">
					<div class="w3-card">
						<header class="w3-container w3-blue w3-center">
						  <h4 style="font-family:Microsoft JhengHei;text-shadow:1px 1px 0 #444">嬰兒用品類</h4>
						</header>
						<div class="w3-container w3-light-gray w3-large">
						  <ul>
							  <li><a href="http://www.akachan.jp/"  target="_blank">Akachan本舖</a></li>
							  <li><a href="https://wowma.jp/nishimatsuya/?aff_id=nsm1001"  target="_blank">西松屋</a></li>
							  <li><a href="https://www.s-birthday.com/special/futafuta/"  target="_blank">Birthday</a></li>
							  <li><a href="http://www.bellemaison.jp/ep/srvlt/EPFB00/EPFB0002/dMidCatLstShow"  target="_blank">Belle Maison</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-3">
				<div class="py-2">
					<div class="w3-card">
						<header class="w3-container w3-blue w3-center">
						  <h4 style="font-family:Microsoft JhengHei;text-shadow:1px 1px 0 #444">日本潮鞋類</h4>
						</header>
						<div class="w3-container w3-light-gray w3-large">
						  <ul>
							  <li><a href="http://www.atmos-tokyo.com/shop/"  target="_blank">Atmos</a></li>
							  <li><a href="http://www.billys-tokyo.net/shop/"  target="_blank">Billys</a></li>
							  <li><a href="http://store.kickslab.com/"  target="_blank">Kicks lab</a></li>
							  <li><a href="http://undefeated.jp/"  target="_blank">Undefeated</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		
	<div class="container mt-3">
		<div class="row">
			<div class="col-xs-12 col-sm-3">
				<div class="py-2">
					<div class="w3-card">
						<header class="w3-container w3-blue w3-center">
						  <h4 style="font-family:Microsoft JhengHei;text-shadow:1px 1px 0 #444">動漫模型類</h4>
						</header>
						<div class="w3-container w3-light-gray w3-large">
						  <ul>
							  <li><a href="https://ec.toranoana.shop/tora/ec/cot"  target="_blank">虎之穴</a></li>
							  <li><a href="https://www.animate-onlineshop.jp/"  target="_blank">Animate</a></li>
							  <li><a href="https://www.happinetonline.com/ec/cmShopTopPage1.html"  target="_blank">Happinet</a></li>
							  <li><a href="https://shop.sanrio.co.jp/"  target="_blank">三麗鷗</a></li>
							  <li><a href="https://www.1999.co.jp/"  target="_blank">Hobby search</a></li>
							  <li><a href="http://p-bandai.jp/"  target="_blank">プレミアムバンダイ</a></li>
							  <li><a href="http://www.amiami.jp/"  target="_blank">Amiami</a></li>
							  <li><a href="https://www.disney.co.jp/"  target="_blank">日本迪士尼商品</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-3">
				<div class="py-2">
					<div class="w3-card">
						<header class="w3-container w3-blue w3-center">
						  <h4 style="font-family:Microsoft JhengHei;text-shadow:1px 1px 0 #444">美妝用品類</h4>
						</header>
						<div class="w3-container w3-light-gray w3-large">
						  <ul>
							  <li><a href="http://www.cosme.net/"  target="_blank">@Cosme</a></li>						  
							  <li><a href="https://www.yojiyacosme.com/index.php"  target="_blank">YOJIYA COSME(優佳雅)</a></li>						  
							  <li><a href="https://www.chomotto.com/"  target="_blank">Chomotto</a></li>						  
							  <li><a href="https://www.spexeshop.com/JP/shoppingGuide_populartopicData.php"  target="_blank">Addiction</a></li>						  
							  <li><a href="http://noevirgroup.jp/excel/"  target="_blank">Excel</a></li>						  
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-3">
				<div class="py-2">
					<div class="w3-card">
						<header class="w3-container w3-blue w3-center">
						  <h4 style="font-family:Microsoft JhengHei;text-shadow:1px 1px 0 #444">戶外運動用品類</h4>
						</header>
						<div class="w3-container w3-light-gray w3-large">
						  <ul>
						  	  <li><a href="https://www.montbell.jp/"  target="_blank">mont.bell</a></li>
							  <li><a href="https://ssx.xebio-online.com/ec/cmShopTopPage1.html"  target="_blank">Super Xebio</a></li>
							  <li><a href="https://victoriagolf.xebio-online.com/ec/cmShopTopPage2.html"  target="_blank">高爾夫球用品</a></li>
							  <li><a href="https://victoria.xebio-online.com/ec/cmShopTopPage4.html"  target="_blank">Surfsnow</a></li>
							  <li><a href="https://lbreath.xebio-online.com/ec/cmShopTopPage3.html"  target="_blank">Outdoor</a></li>
							  <li><a href="https://www.nike.com/jp/ja_jp/"  target="_blank">日本Nike</a></li>
							  <li><a href="https://shop.adidas.jp/"  target="_blank">日本adidas</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-3">
				<div class="py-2">
					<div class="w3-card">
						<header class="w3-container w3-blue w3-center">
						  <h4 style="font-family:Microsoft JhengHei;text-shadow:1px 1px 0 #444">包包類</h4>
						</header>
						<div class="w3-container w3-light-gray w3-large">
						  <ul>
							  <li><a href="https://www.tsuchiya-kaban.jp/"  target="_blank">土屋鞄製造所</a></li>
							  <li><a href="https://www.yoshidakaban.com/index.html"  target="_blank">日本Porter包</a></li>
							  <li><a href="http://www.ichizawa.co.jp/"  target="_blank">一澤信三郎帆布包</a></li>
							  <li><a href="http://www.san-mihara.com/"  target="_blank">SAN HIDEAKI MIHARA</a></li>
							  <li><a href="http://www.toffandloadstone.com/"  target="_blank">TOFF&LOADSTONE</a></li>
							  <li><a href="https://www.baobaoisseymiyake.com/shop/baobaoisseymiyake/item/list/category_id/95"  target="_blank">BAO BAO ISSEY MIYAKE</a></li>
							  <li><a href="http://kawa-kawa.com/"  target="_blank">kawa kawa</a></li>
							  <li><a href="http://caledechecli.com/"  target="_blank">calede checli</a></li>
							  <li><a href="http://naotosatoh.jp/archives/category/item"  target="_blank">NAOTO SATOH</a></li>
							  <li><a href="https://genten-onlineshop.jp/pc/"  target="_blank">genten</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
		<div class="container mt-3">
			<div class="row">
			<div class="col-xs-12 col-sm-3">
				<div class="py-2">
					<div class="w3-card">
						<header class="w3-container w3-blue w3-center">
						  <h4 style="font-family:Microsoft JhengHei;text-shadow:1px 1px 0 #444">雜貨文具類</h4>
						</header>
						<div class="w3-container w3-light-gray">
						  <ul>
							  <li><a href="https://loft.omni7.jp/top"  target="_blank">LOFT </a></li>
							  <li><a href="https://webshop.sekaido.co.jp/"  target="_blank">世界堂</a></li>
							  <li><a href="https://www.midori-store.net/"  target="_blank">MIDORI</a></li>
							  <li><a href="http://store.ito-ya.co.jp/"  target="_blank">伊東屋ITOYA</a></li>
							  <li><a href="https://shop.masking-tape.jp/"  target="_blank">MT紙膠帶</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-3">
				<div class="py-2">
					<div class="w3-card">
						<header class="w3-container w3-blue w3-center">
						  <h4 style="font-family:Microsoft JhengHei;text-shadow:1px 1px 0 #444">日本傳統工藝品</h4>
						</header>
						<div class="w3-container w3-light-gray w3-large">
						 <ul>
							  <li><a href="https://japan-design.imazy.net/jp/"  target="_blank">日本design store</a></li>
							  <li><a href="https://www.yoshidakaban.com/index.html"  target="_blank">日本Porter包</a></li>
							  <li><a href="http://www.tadanosuke.jp/products/list.php"  target="_blank">忠之助商店</a></li>
							  <li><a href="http://kougeihin.jp/onlineshop/"  target="_blank">青山SQUARE</a></li>
							  <li><a href="https://www.alexcious.com/ja/"  target="_blank">Alexcious</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-3">
				<div class="py-2">
					<div class="w3-card">
						<header class="w3-container w3-blue w3-center">
						  <h4 style="font-family:Microsoft JhengHei;text-shadow:1px 1px 0 #444">流行服飾類</h4>
						</header>
						<div class="w3-container w3-light-gray w3-large">
						 <ul>
							  <li><a href="https://e.right-on.co.jp/"  target="_blank">Right-On</a></li>
							  <li><a href="http://www.beams.co.jp/"  target="_blank">Beams</a></li>
							  <li><a href="http://www.dot-st.com/rageblue/"  target="_blank">Rage Blue</a></li>
							  <li><a href="https://www.ec-store.net/category/SLY"  target="_blank">SLY</a></li>
							  <li><a href="https://www.blackbymoussy.jp/online/"  target="_blank">Black by moussy</a></li>
							  <li><a href="https://www.ec-store.net/category/MOUSSY"  target="_blank">moussy</a></li>
							  <li><a href="http://www.wego.jp/"  target="_blank">WEGO </a></li>
							  <li><a href="http://www.jadore-jun.jp/vis/"  target="_blank">VIS</a></li>						  
							  <li><a href="https://store.saneibd.com/naturalbeautybasic/"  target="_blank">Natural Beauty Basic</a></li>						  
							  <li><a href="http://www.freaksstore.com/"  target="_blank">Freak's Store</a></li>						  
							  <li><a href="https://ailand-store.jp/cecilmcbee/"  target="_blank">Cecil McBee</a></li>						  
							</ul>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
	</div>
	</div>
	
	</div>
	</div> <!-- background image end -->
	
	<div class="container-fluid w3-red" style="height:8px;">
	</div>
	<div class="container-fluid w3-dark-gray ">
		<div class="row">
			<div class="container my-5">
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="d-flex flex-column">
						  <div class="p-2">
						  		<img class="w3-circle w3-card-4" src="img/eason_logo.jpg">
						  </div>
						  <div class="p-2">
						       <b class="w3-large ml-5 pl-4">Eason代購網</b>	
						  </div>
						</div>
					</div>
		   			<div class="col-xs-12 col-sm-8">
		   				<div class="d-flex justify-content-between">
		   					<div class="align-self-center w3-large">
			   					<b>歡迎加入Eason LINE@</b><br>
								<b>搜索ID  @cfd3755c 或請掃描 QRcode</b><br>					
								<b>2019 Copyright © All Rights Reserved</b>
								<br>
								<br>
								<a  href="https://line.me/R/ti/p/%40cfd3755c" target="_blank"><img class="w3-card" height="36" border="0" alt="加入好友" src="https://scdn.line-apps.com/n/line_add_friends/btn/zh-Hant.png"></a>
			   				</div>
			   				<div class="align-self-center">
			   					<img src="http://qr-official.line.me/L/5si8mM9h4l.png" class="w3-image">
			   				</div>
		   				</div>
		   			</div>
				</div>
			</div>
		</div>
	</div>







	


	 




	
	
	<%@include file="includ/footer.jsp" %>
    
    <script type="text/javascript">
 // Get the modal
    var loginModal = document.getElementById('loginModal');
    var registerModal = document.getElementById('registerModal');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
      	if (event.target == loginModal || event.target == registerModal) {
       	loginModal.style.display = "none";
       	registerModal.style.display = "none";		   
      	}		  	
    }

    //試算表
    function firstPrice(){		
    	var producePrice = $( '#productPrice' ).val();
    	var japFare = $( '#japFare' ).val();				//日本運費
//    	var jpaTransfer = $( '#jpaTransfer' ).val();		//400
    	var rate = $( '#rate' ).val();						//0.03
    	var serviceFee = $( '#serviceFee' ).val();			//20台幣
    	var secondCount = $( '#secondCount' ).text();
    	
    	if(japFare == '' || producePrice == '' || serviceFee == ''){
    		$( '#firstConut' ).text("計算中");			
    	}else{
    		var firstCount = Math.ceil((parseInt(producePrice) + parseInt(japFare))*rate + parseInt(serviceFee));
    		$( '#firstConut' ).text(firstCount);
    		if(!isNaN(parseInt(secondCount))  && !isNaN(parseInt(firstCount))){
    			var totalCount = parseInt(firstCount) + parseInt(secondCount);
    			$( '#totalCount' ).text(totalCount);
    		}
    	}
    	
    }

    $(document).ready(function() {
    	var jpyRate = $( '#jypRate' ).text();
    	$( '#rate' ).val(jpyRate);
    	$( '#rate2' ).val(jpyRate);
//    	secondPrice();
//    	$('#productPrice').change(function(){
//    		var producePrice = $( '#productPrice' ).val();		
//    		firstPrice();
//    	});
    	
    	
    });

    function secondPrice(){
    	var InternalPrice = $( '#InternalPrice' ).val();		//國際運費
//    	var twFare = $( '#twFare' ).val();						//台運
    	var rate = $( '#rate2' ).val();							//匯率
    	var firstCount = $( '#firstConut' ).text();
    	if(InternalPrice == ''){
    		$( '#secondCount' ).text("計算中");			
    	}else{
    		var secondCount = Math.ceil(InternalPrice*rate);
    		$( '#secondCount' ).text(secondCount);
    		
    		if(!isNaN(parseInt(secondCount))  && !isNaN(parseInt(firstCount))){
    			var totalCount = parseInt(firstCount) + parseInt(secondCount);
    			$( '#totalCount' ).text(totalCount);
    		}
    	}
    	
    }



    </script> 
    
</body>
</html>