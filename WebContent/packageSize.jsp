<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE>
<html>
    <head>
    	<%@include file="includ/header.jsp" %>
  	</head>	
  	
  <body style="font-family:Microsoft JhengHei,monospace;"  >
  
	<!--navbar ==============================================================-->
	<%@include file="includ/navbar.jsp" %>
	
	<!-- ===========內容===================== -->
<!-- end-->
	<div class="container-fluid w3-light-gray">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<header class="w3-container w3-theme w3-center w3-bottombar w3-border-red w3-wide my-4" >
				  <h1 class="w3-xxlarge" style="text-shadow:1px 1px 0 #444">商品重量大小限制</h1>
				</header>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="py-2">
					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card">
						<div class="mb-3">
						  <h2>A.EMS/空運/海運</h2>
						  <ul class="w3-large">
						  		<li>重量限制為30KG以內</li>
						  		<li>包裹最長邊不可超過 150CM、長 + (寬 X 2+ 高 X 2)三邊總和不可超過 300CM</li>
						  		<li>
						  			<img src="img/ems.jpg" class="w3-image">
						  		</li>
						  </ul>
					  	</div>
					</div>
				</div>
			</div>
			<!--<div class="col-xs-12 col-sm-6">
				<div class="py-2">
					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card">
						<div class="w3-xlarge mb-3">
						  <h2>B.集貨海運 (包關稅)</h2>
						  <ul class="w3-large">
						  		<li>重量限制為10KG以上30KG以內</li>
							    <li>包裹最長邊不可超過 150CM、長 + (寬 X 2+ 高 X 2)三邊總和不可超過 300CM</li>
							    <li>
							    	<img src="img/seaTran.jpg" class="w3-image">
							    </li>
						  </ul>
					  	</div>
					</div>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-6">
				<div class="py-2">
					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card">
						<div class="w3-xlarge mb-3">
						  <h2>C.集貨空運 (包關稅)</h2>
						  <ul class="w3-large">
						  		<li>重量限制為20KG以內</li>
							    <li>包裹長寬高其中一邊不可超過 80CM、或長寬高三邊總和不可超過 150CM</li>
							    <li>
							    	<img src="img/packageSizeNew.png" class="w3-image">
							    </li>
							    <li><font class="w3-text-red">體積重</font>與<font class="w3-text-red">實重</font>比例不可超過<font class="w3-text-red">5倍</font></li>
							    <li>體積重計算方法：(長cm X 闊cm X 高cm) / 6000 X 2.2046</li>
							    <li>例如：	體積重 (80cm X 40cm X 30cm) / 6000 X 2.2046 = 35.3KG<br>
							    	<font class="w3-text-red">實重</font>就要<font class="w3-text-red">大於</font> 7.05KG (35.3 / 5 = 7.05kg)</li>
							    <li>如超出以上其中一項限制，有機會需要付出額外費用</li>
							    <li>常見超出比例的物品如下：<br>
							    大型玩具、公仔、布偶、嬰兒車、腳踏車、寢具類商品等….</li>
						  </ul>
					  	</div>
					</div>
				</div>
			</div> -->
			<div class="col-xs-12 col-sm-6">
				<div class="py-2">
					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card">
						<div class="w3-xlarge mb-3">
						  <h2>B.空運小包</h2>
						  <ul class="w3-large">
						  		<li>重量限制為2KG以內</li>
						  		<li>包裹尺寸請參考下圖</li>
						  		<li>
						  			<img src="img/smallpack.jpg" class="w3-image">
						  		</li>
						  </ul>
					  	</div>
					</div>
				</div>
			</div>
			<!--<div class="col-xs-12 col-sm-6">
				<div class="py-2">
					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card">
						<div class="w3-xlarge mb-3">
						  <h2>C.台灣運費與超商查詢</h2>
						  <ul class="w3-large">
						  		<li>超商取貨60台幣</li>
							    <li>
							    	<b>超商查詢(點擊開啟連結)：</b>
							    	<ul>
							    		<li><a class="w3-text-blue" href="https://reurl.cc/DxA8N" target="_blank">全家</a></li>
							    		<li><a class="w3-text-blue" href="https://reurl.cc/R2zEz" target="_blank">萊爾富</a></li>
							    		<li><a class="w3-text-blue" href="https://reurl.cc/531Zy" target="_blank">7-11</a></li>
							    	</ul>
							    </li>
							    <li>
							    	<b>宅配：</b>
							    	<ul>
							    		<li>重量不得超過5公斤；100台幣</li>
							    		<li>重量不得超過10公斤；125台幣</li>
							    		<li>重量不得超過15公斤；150台幣</li>
							    		<li>重量不得超過20公斤；180台幣</li>
							    	</ul>
							    </li>
						  </ul>
					  	</div>
					</div>
				</div>
			</div> -->
			
		</div>
	</div>


	
	<div class="container-fluid w3-red" style="height:8px;">
	</div>
	<div class="container-fluid w3-dark-gray ">
		<div class="row">
			<div class="container my-5">
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="d-flex flex-column">
						  <div class="p-2">
						  		<img class="w3-circle w3-card-4" src="img/eason_logo.jpg">
						  </div>
						  <div class="p-2">
						       <b class="w3-large ml-5 pl-4">Eason代購網</b>	
						  </div>
						</div>
					</div>
		   			<div class="col-xs-12 col-sm-8">
		   				<div class="d-flex justify-content-between">
		   					<div class="align-self-center w3-large">
			   					<b>歡迎加入Eason LINE@</b><br>
								<b>搜索ID  @cfd3755c 或請掃描 QRcode</b><br>					
								<b>2019 Copyright © All Rights Reserved</b>
								<br>
								<br>
								<a  href="https://line.me/R/ti/p/%40cfd3755c" target="_blank"><img class="w3-card" height="36" border="0" alt="加入好友" src="https://scdn.line-apps.com/n/line_add_friends/btn/zh-Hant.png"></a>
			   				</div>
			   				<div class="align-self-center">
			   					<img src="http://qr-official.line.me/L/5si8mM9h4l.png" class="w3-image">
			   				</div>
		   				</div>
		   			</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	
	
	
	
	
	
	
	<!-- ===========內容===================== -->
	<%@include file="includ/footer.jsp" %>
</body>
</html>