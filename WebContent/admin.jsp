<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<%
	String twdjpyRate = "";
	twdjpyRate = (String)request.getServletContext().getAttribute("TWDJPYRate");
	if(twdjpyRate == null || twdjpyRate.length() == 0 ){
		twdjpyRate = "---";
	}
%>
<!DOCTYPE html>
<html>
<head>
<%@include file="includ/header.jsp" %>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-12">
				<h4>手動匯率調整：</h4>
				<div>目前匯率：<%=twdjpyRate %></div>
			</div>
			<div class="col-xs-12 col-sm-12 col-12">
				<form action="${pageContext.request.contextPath}/adminController" method="post">
					<input type="text" name="rate">
					<input type="submit" value="送出"></submit>
				</form>
				<a href="index.jsp">回首頁查看</a>
			</div>
		</div>
	</div>
</body>
</html>