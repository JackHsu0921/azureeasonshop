<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE>
<html>
    <head>
    	<%@include file="includ/header.jsp" %>
  	</head>	
  	
  <body style="font-family:Microsoft JhengHei,monospace;" class="w3-light-gray" >
  
	<!--navbar ==============================================================-->
	<%@include file="includ/navbar.jsp" %>
	
	<!-- ===========內容===================== -->
<!-- end-->
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<header class="w3-container w3-theme w3-center w3-bottombar w3-border-red w3-wide my-4" >
				  <h1 class="w3-xxlarge" style="text-shadow:1px 1px 0 #444">運費價格表</h1>
				</header>
			</div>
				
				<div class="col-xs-12 col-sm-4 ">
					<div class="py-2">
						<table class="w3-table-all w3-large w3-border-gray w3-hoverable">
						    <tr class="w3-blue">
						      <th colspan="2" class="w3-center">日本郵政(空運小包)</th>
						    </tr>
						    <tr>
						      <th colspan="2" class="w3-center">最大重量2Kg</th>
						    </tr>
						     <tr>
						      <th colspan="2" class="w3-center">日本寄出後約6~8天到台灣</th>
						    </tr>
						    <tr>
						      <td class="w3-center">重量</td>
						      <td class="w3-center">費用</td>					      
						    </tr>
						    <tr>
						      <td class="w3-center">不超過50g</td>
						      <td class="w3-center">530日圓</td>
						    </tr>
						    <tr>
						      <td class="w3-center">不超過100g</td>
						      <td class="w3-center">600日圓</td>
						    </tr>
						    <tr>
						      <td class="w3-center">不超過150g</td>
						      <td class="w3-center">670日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過200g</td>
						      <td class="w3-center">740日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過250g</td>
						      <td class="w3-center">810日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過300g</td>
						      <td class="w3-center">880日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過350g</td>
						      <td class="w3-center">950日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過400g</td>
						      <td class="w3-center">1,020日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過450g</td>
						      <td class="w3-center">1,090日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過500g</td>
						      <td class="w3-center">1,160日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過550g</td>
						      <td class="w3-center">1,230日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過600g</td>
						      <td class="w3-center">1,300日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過650g</td>
						      <td class="w3-center">1,370日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過700g</td>
						      <td class="w3-center">1,440日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過750g</td>
						      <td class="w3-center">1,510日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過800g</td>
						      <td class="w3-center">1,580日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過850g</td>
						      <td class="w3-center">1,650日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過900g</td>
						      <td class="w3-center">1,720日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過950g</td> 
						      <td class="w3-center">1,790日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過1000g</td>
						      <td class="w3-center">1,860日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過1250g</td>
						      <td class="w3-center">2,035日圓</td>
						    </tr>
						    <tr>
						      <td class="w3-center">不超過1500g</td>
						      <td class="w3-center">2,210日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過1750g</td>
						      <td class="w3-center">2,385日圓</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過2000g</td>
						      <td class="w3-center">2,560日圓</td>
						    </tr>
						   
					    </table>
					</div>
				</div>
				
				<!-- <div class="col-xs-12 col-sm-4 ">
					<div class="py-2">
						<table class="w3-table-all w3-large w3-border-gray w3-hoverable">
						    <tr class="w3-blue">
						      <th colspan="2" class="w3-center">集貨空運(包關稅)</th>
						    </tr>
						    <tr>
						      <th colspan="2" class="w3-center">最大重量20Kg</th>
						    </tr>
						     <tr>
						      <th colspan="2" class="w3-center">日本寄出後2週到台灣</th>
						    </tr>
						    <tr>
						      <td class="w3-center">重量</td>
						      <td class="w3-center w3-text-red">費用(每0.5kg/150台幣 )</td>					      
						    </tr>
						    <tr>
						      <td class="w3-center">不超過0.5kg</td>
						      <td class="w3-center">150台幣</td>
						    </tr>
						    <tr>
						      <td class="w3-center">不超過1kg</td>
						      <td class="w3-center">300台幣</td>
						    </tr>
						    <tr>
						      <td class="w3-center">不超過1.5kg</td>
						      <td class="w3-center">450台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過2kg</td>
						      <td class="w3-center">600台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過2.5kg</td>
						      <td class="w3-center">750台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過3kg</td>
						      <td class="w3-center">900台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過3.5kg</td>
						      <td class="w3-center">1050台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過4kg</td>
						      <td class="w3-center">1,200台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過4.5kg</td>
						      <td class="w3-center">1,350台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過5kg</td>
						      <td class="w3-center">1,500台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過5.5kg</td>
						      <td class="w3-center">1,650台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過6kg</td>
						      <td class="w3-center">1,800台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過6.5kg</td>
						      <td class="w3-center">1,950台幣</td>
						    </tr>
						  	
						  	<tr>
						      <td class="w3-center">不超過7kg</td>
						      <td class="w3-center">2,100台幣</td>
						    </tr>
						  	
						   	<tr>
						      <td class="w3-center">不超過7.5kg</td>
						      <td class="w3-center">2,250台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過8kg</td>
						      <td class="w3-center">2,400台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過8.5kg</td>
						      <td class="w3-center">2,550台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過9kg</td>
						      <td class="w3-center">2,700台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過9.5kg</td>
						      <td class="w3-center">2,850台幣</td>
						    </tr>
						  
						    <tr>
						      <td class="w3-center">不超過10kg</td>
						      <td class="w3-center">3,000台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過10.5kg</td>
						      <td class="w3-center">3,150台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過11kg</td>
						      <td class="w3-center">3,300台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過11.5kg</td>
						      <td class="w3-center">3,450台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過12kg</td>
						      <td class="w3-center">3,600台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過12.5kg</td>
						      <td class="w3-center">3,750台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過13kg</td>
						      <td class="w3-center">3,900台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過13.5kg</td>
						      <td class="w3-center">4,050台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過14kg</td>
						      <td class="w3-center">4,200台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過14.5kg</td>
						      <td class="w3-center">4,350台幣</td>
						    </tr>
						    
						     <tr>
						      <td class="w3-center">不超過15kg</td>
						      <td class="w3-center">4,500台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過15.5kg</td>
						      <td class="w3-center">4,650台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過16kg</td>
						      <td class="w3-center">4,800台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過16.5kg</td>
						      <td class="w3-center">4,950台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過17kg</td>
						      <td class="w3-center">5,100台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過17.5kg</td>
						      <td class="w3-center">5,250台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過18kg</td>
						      <td class="w3-center">5,400台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過18.5kg</td>
						      <td class="w3-center">5,550台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過19kg</td>
						      <td class="w3-center">5,700台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過19.5kg</td>
						      <td class="w3-center">5,850台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過20kg</td>
						      <td class="w3-center">6,000台幣</td>
						    </tr>
					    </table>
					</div>
				</div>-->
				
				<!--<div class="col-xs-12 col-sm-4 ">
					<div class="py-2">
						<table class="w3-table-all w3-large w3-border-gray w3-hoverable">
						    <tr class="w3-blue">
						      <th colspan="2" class="w3-center">集貨海運(包關稅)</th>
						    </tr>
						    <tr>
						      <th colspan="2" class="w3-center">重量限制為10KG以上30KG以內</th>
						    </tr>
						     <tr>
						      <th colspan="2" class="w3-center">日本寄出後約1~3個月到台灣</th>
						    </tr>
						    <tr>
						      <td class="w3-center">重量</td>
						      <td class="w3-center w3-text-red">費用(每1kg/150台幣 )</td>					      
						    </tr>
						   						  
						    <tr>
						      <td class="w3-center">不超過11.0kg</td>
						      <td class="w3-center">1,650台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過12.0kg</td>
						      <td class="w3-center">1,800台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過13.0kg</td>
						      <td class="w3-center">1,950台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過14.0kg</td>
						      <td class="w3-center">2,100台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過15.0kg</td>
						      <td class="w3-center">2,250台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過16.0kg</td>
						      <td class="w3-center">2,400台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過17.0kg</td>
						      <td class="w3-center">2,550台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過18.0kg</td>
						      <td class="w3-center">2,700台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過19.0kg</td>
						      <td class="w3-center">2,850台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過20.0kg</td>
						      <td class="w3-center">3,000台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過21.0kg</td>
						      <td class="w3-center">3,150台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過22.0kg</td>
						      <td class="w3-center">3,300台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過23.0kg</td>
						      <td class="w3-center">3,450台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過24.0kg</td>
						      <td class="w3-center">3,600台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過25.0kg</td>
						      <td class="w3-center">3,750台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過26.0kg</td>
						      <td class="w3-center">3,900台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過27.0kg</td>
						      <td class="w3-center">4,050台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過28.0kg</td>
						      <td class="w3-center">4,200台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過29.0kg</td>
						      <td class="w3-center">4,350台幣</td>
						    </tr>
						    
						    <tr>
						      <td class="w3-center">不超過30.0kg</td>
						      <td class="w3-center">4,500台幣</td>
						    </tr>
						    					  
						   
					    </table>
					</div>
				</div> -->
			
			 <div class="col-xs-12 col-sm-4">
		   		<div class="py-2 mb-3">
		   			<table class="w3-table-all w3-large w3-border-gray w3-hoverable">
					    <tr class="w3-blue" >
					      <th colspan="2" class="w3-center">日本郵政 EMS</th>
					    </tr>
					    <tr >
					      <th colspan="2" class="w3-center">最大重量30KG</th>
					    </tr>
					    <tr >
					      <th colspan="2" class="w3-center">日本寄出後約3~4天到台灣</th>
					    </tr>
					    <tr>
					      <th class="w3-center">重量</th>
					      <th class="w3-center">費用</th>					      
					    </tr>
					   
					    <tr>
					      <td class="w3-center">不超過500g</td>
					      <td class="w3-center">1,400日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過600g</td>
					      <td class="w3-center">1,540日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過700g</td>
					      <td class="w3-center">1,680日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過800g</td>
					      <td class="w3-center">1,820日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過900g</td>
					      <td class="w3-center">1,960日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過1.0kg</td>
					      <td class="w3-center">2,100日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過1.25kg</td>
					      <td class="w3-center">2,400日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過1.5kg</td>
					      <td class="w3-center">2,700日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過1.75kg</td>
					      <td class="w3-center">3,000日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過2.5kg</td>
					      <td class="w3-center">3,800日圓</td>
					    </tr>					  
					  
					    <tr>
					      <td class="w3-center">不超過3.0kg</td>
					      <td class="w3-center">4,300日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過3.5kg</td>
					      <td class="w3-center">4,800日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過4.0kg</td>
					      <td class="w3-center">5,300日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過4.5kg</td>
					      <td class="w3-center">5,800日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過5.0kg</td>
					      <td class="w3-center">6,300日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過5.5kg</td>
					      <td class="w3-center">6,800日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過6.0kg</td>
					      <td class="w3-center">7,300日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過7.0kg</td>
					      <td class="w3-center">8,100日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過8.0kg</td>
					      <td class="w3-center">8,900日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過9.0kg</td>
					      <td class="w3-center">9,700日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過10.0kg</td>
					      <td class="w3-center">10,500日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過11.0kg</td>
					      <td class="w3-center">11,300日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過12.0kg</td>
					      <td class="w3-center">12,100日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過13.0kg</td>
					      <td class="w3-center">12,900日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過14.0kg</td>
					      <td class="w3-center">13,700日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過15.0kg</td>
					      <td class="w3-center">14,500日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過16.0kg</td>
					      <td class="w3-center">15,300日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過17.0kg</td>
					      <td class="w3-center">16,100日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過18.0kg</td>
					      <td class="w3-center">16,900日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過19.0kg</td>
					      <td class="w3-center">17,700日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過20.0kg</td>
					      <td class="w3-center">18,500日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過21.0kg</td>
					      <td class="w3-center">19,300日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過22.0kg</td>
					      <td class="w3-center">20,100日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過23.0kg</td>
					      <td class="w3-center">20,900日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過24.0kg</td>
					      <td class="w3-center">21,700日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過25.0kg</td>
					      <td class="w3-center">22,500日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過26.0kg</td>
					      <td class="w3-center">23,300日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過27.0kg</td>
					      <td class="w3-center">24,100日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過28.0kg</td>
					      <td class="w3-center">24,900日圓</td>
					    </tr>
					    
					    <tr>
					      <td class="w3-center">不超過29.0kg</td>
					      <td class="w3-center">25,700日圓</td>
					    </tr>
					    
					     <tr>
					      <td class="w3-center">不超過30.0kg</td>
					      <td class="w3-center">26,500日圓</td>
					    </tr>
					  					  
					  </table>
   				</div>
		   </div>
			
			
		   <div class="col-xs-12 col-sm-4">
		   		<div class="py-2 mb-3">
		   			<table class="w3-table-all w3-large w3-border-gray w3-hoverable">
					    <tr class="w3-blue" >
					      <th colspan="2" class="w3-center">日本郵政 空運</th>
					    </tr>
					    <tr >
					      <th colspan="2" class="w3-center">最大重量30KG</th>
					    </tr>
					    <tr >
					      <th colspan="2" class="w3-center">日本寄出後約6~8天到台灣</th>
					    </tr>
					    <tr>
					      <th class="w3-center">重量</th>
					      <th class="w3-center">費用</th>					      
					    </tr>
					   
					    <tr>
					      <td class="w3-center">不超過500g</td>
					      <td class="w3-center">1,700日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過1.0kg</td>
					      <td class="w3-center">2,050日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過1.5kg</td>
					      <td class="w3-center">2,400日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過2.0kg</td>
					      <td class="w3-center">2,750日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過2.5kg</td>
					      <td class="w3-center">3,100日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過3.0kg</td>
					      <td class="w3-center">3,450日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過3.5kg</td>
					      <td class="w3-center">3,800日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過4.0kg</td>
					      <td class="w3-center">4,150日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過4.5kg</td>
					      <td class="w3-center">4,500日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過5.0kg</td>
					      <td class="w3-center">4,850日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過5.5kg</td>
					      <td class="w3-center">5,150日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過6.0kg</td>
					      <td class="w3-center">5,450日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過6.5kg</td>
					      <td class="w3-center">5,750日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過7.0kg</td>
					      <td class="w3-center">6,050日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過7.5kg</td>
					      <td class="w3-center">6,350日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過8.0kg</td>
					      <td class="w3-center">6,650日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過8.5kg</td>
					      <td class="w3-center">6,950日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過9.0kg</td>
					      <td class="w3-center">7,250日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過9.5kg</td>
					      <td class="w3-center">7,550日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過10.0kg</td>
					      <td class="w3-center">7,850日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過11.0kg</td>
					      <td class="w3-center">8,250日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過12.0kg</td>
					      <td class="w3-center">8,650日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過13.0kg</td>
					      <td class="w3-center">9,050日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過14.0kg</td>
					      <td class="w3-center">9,450日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過15.0kg</td>
					      <td class="w3-center">9,850日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過16.0kg</td>
					      <td class="w3-center">10,250日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過17.0kg</td>
					      <td class="w3-center">10,650日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過18.0kg</td>
					      <td class="w3-center">11,050日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過19.0kg</td>
					      <td class="w3-center">11,450日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過20.0kg</td>
					      <td class="w3-center">11,850日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過21.0kg</td>
					      <td class="w3-center">12,250日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過22.0kg</td>
					      <td class="w3-center">12,650日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過23.0kg</td>
					      <td class="w3-center">13,050日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過24.0kg</td>
					      <td class="w3-center">13,450日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過25.0kg</td>
					      <td class="w3-center">13,850日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過26.0kg</td>
					      <td class="w3-center">14,250日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過27.0kg</td>
					      <td class="w3-center">14,650日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過28.0kg</td>
					      <td class="w3-center">15,050日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過29.0kg</td>
					      <td class="w3-center">15,450日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過30.0kg</td>
					      <td class="w3-center">15,850日圓</td>
					    </tr>
					  					  
					  </table>
   				</div>
		   </div>
		   
		   <div class="col-xs-12 col-sm-4">
		   		<div class="py-2">
		   			<table class="w3-table-all w3-border-gray w3-large w3-hoverable">
					    <tr class="w3-blue">
					      <th colspan="2" class="w3-center">日本郵政 海運</th>
					    </tr>
					    <tr >
					      <th colspan="2" class="w3-center">最大重量30kg</th>
					    </tr>
					    <tr >
					      <th colspan="2" class="w3-center">寄出後約1~3個月到台灣</th>
					    </tr>
					    <tr>
					      <td class="w3-center">重量</td>
					      <td class="w3-center">金額</td>					      
					    </tr>
					    <tr>
					      <td class="w3-center">不超過500g</td>
					      <td class="w3-center" rowspan="2" style="vertical-align : middle;text-align:center;">1,600日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過1.0kg</td>
					    </tr>
					   <tr>
					      <td class="w3-center">不超過1.5kg</td>
					      <td class="w3-center" rowspan="2" style="vertical-align : middle;text-align:center;">1,900日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過2.0kg</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過2.5kg</td>
					      <td class="w3-center" rowspan="2" style="vertical-align : middle;text-align:center;">2,200日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過3.0kg</td>
					    </tr>
					  
					   <tr>
					      <td class="w3-center">不超過3.5kg</td>
					      <td class="w3-center"  rowspan="2" style="vertical-align : middle;text-align:center;">2,500日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過4.0kg</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過4.5kg</td>
					      <td class="w3-center" rowspan="2" style="vertical-align : middle;text-align:center;">2,800日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過5.0kg</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過5.5kg</td>
					      <td class="w3-center" rowspan="2" style="vertical-align : middle;text-align:center;">3,100日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過6.0kg</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過6.5kg</td>
					      <td class="w3-center" rowspan="2" style="vertical-align : middle;text-align:center;">3,400日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過7.0kg</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過7.5kg</td>
					      <td class="w3-center" rowspan="2" style="vertical-align : middle;text-align:center;">3,700日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過8.0kg</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過8.5kg</td>
					      <td class="w3-center" rowspan="2" style="vertical-align : middle;text-align:center;">4,000日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過9.0kg</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過9.5kg</td>
					      <td class="w3-center" rowspan="2" style="vertical-align : middle;text-align:center;">4,300日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過10.0kg</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過11.0kg</td>
					      <td class="w3-center">4,550日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過12.0kg</td>
					      <td class="w3-center">4,800日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過13.0kg</td>
					      <td class="w3-center">5,050日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過14.0kg</td>
					      <td class="w3-center">5,300日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過15.0kg</td>
					      <td class="w3-center">5,550日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過16.0kg</td>
					      <td class="w3-center">5,800日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過17.0kg</td>
					      <td class="w3-center">6,050日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過18.0kg</td>
					      <td class="w3-center">6,300日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過19.0kg</td>
					      <td class="w3-center">6,550日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過20.0kg</td>
					      <td class="w3-center">6,800日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過21.0kg</td>
					      <td class="w3-center">7,050日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過22.0kg</td>
					      <td class="w3-center">7,300日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過23.0kg</td>
					      <td class="w3-center">7,550日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過24.0kg</td>
					      <td class="w3-center">7,800日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過25.0kg</td>
					      <td class="w3-center">8,050日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過26.0kg</td>
					      <td class="w3-center">8,300日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過27.0kg</td>
					      <td class="w3-center">8,550日圓</td>
					    </tr>
					    <tr>
					      <td class="w3-center">不超過28.0kg</td>
					      <td class="w3-center">8,800日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過29.0kg</td>
					      <td class="w3-center">9,050日圓</td>
					    </tr>
					  
					    <tr>
					      <td class="w3-center">不超過30.0kg</td>
					      <td class="w3-center">9,300日圓</td>
					    </tr>
					  					   
					  </table>
   				</div>
		   </div>
		</div>
	</div>


	
	<div class="container-fluid w3-red" style="height:8px;">
	</div>
	<div class="container-fluid w3-dark-gray ">
		<div class="row">
			<div class="container my-5">
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="d-flex flex-column">
						  <div class="p-2">
						  		<img class="w3-circle w3-card-4" src="img/eason_logo.jpg">
						  </div>
						  <div class="p-2">
						       <b class="w3-large ml-5 pl-4">Eason代購網</b>	
						  </div>
						</div>
					</div>
		   			<div class="col-xs-12 col-sm-8">
		   				<div class="d-flex justify-content-between">
		   					<div class="align-self-center w3-large">
			   					<b>歡迎加入Eason LINE@</b><br>
								<b>搜索ID  @cfd3755c 或請掃描 QRcode</b><br>					
								<b>2019 Copyright © All Rights Reserved</b>
								<br>
								<br>
								<a  href="https://line.me/R/ti/p/%40cfd3755c" target="_blank"><img class="w3-card" height="36" border="0" alt="加入好友" src="https://scdn.line-apps.com/n/line_add_friends/btn/zh-Hant.png"></a>
			   				</div>
			   				<div class="align-self-center">
			   					<img src="http://qr-official.line.me/L/5si8mM9h4l.png" class="w3-image">
			   				</div>
		   				</div>
		   			</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	
	
	
	
	
	
	
	<!-- ===========內容===================== -->
	<%@include file="includ/footer.jsp" %>
</body>
</html>