//Swal
$( "#sendMessageBtn" ).click(function(){
   	swal({
		title: '確定要傳送嗎?',
	  	text: "將會立即傳送給所以好友!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, 傳送!'
	}).then((result) => {
		if (result.value) {
			swal(
				'成功!',
				'訊息已傳送完成',
				'success'
			)
		}
	})
});//sendMessageBtn end

//DatetimePicker
$( "#datepicker" ).datetimepicker({
	dateFormat: 'yy-mm-dd',
	numberOfMonths: 2,
    showButtonPanel: true,
	changeMonth: true,
	showAnim: 'fold',
	minDate: 0,
	maxDate:'+1Y',		    	
    changeYear: true      			
});
$( "#datepicker" ).datepicker( $.datepicker.regional[ "zh-TW" ] );
		
//核取方塊
$("#agentBox").click(function(){
	if($("#agentBox").prop("checked")){
		$("input[name='agent']").prop("checked", true);
	}else{
		$("input[name='agent']").prop("checked", false);
	}				
});
$("#ticketCenterBox").click(function(){
	if($("#ticketCenterBox").prop("checked")){
		$("input[name='ticketCenter']").prop("checked", true);
	}else{
		$("input[name='ticketCenter']").prop("checked", false);
	}				
});
$("#areaBox").click(function(){
	if($("#areaBox").prop("checked")){
		$("input[name='area']").prop("checked", true);
	}else{
		$("input[name='area']").prop("checked", false);
	}				
});
$("#otherBox").click(function(){
	if($("#otherBox").prop("checked")){
		$("input[name='other']").prop("checked", true);
	}else{
		$("input[name='other']").prop("checked", false);
	}				
});