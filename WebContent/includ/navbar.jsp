<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>

<!--navbar ==============================================================-->
		<nav class="navbar navbar-expand-lg navbar-dark w3-topbar w3-border-red w3-dark-gray  w3-xlarge" role="navigation">
			<a class="navbar-brand mb-0 h1" href="index.jsp">										<!--公司行號、產品名稱-->
				<img src="img/eason_logo.jpg"  class="d-inline-block align-self-end w3-circle w3-hide-small" style="max-width:80px;" alt="eason">
				<b class="w3-xlarge">
					伊森日本網站代購
				</b>
			</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">
				<!--ml-auto mr-auto 改變對齊位置-->
				<!--<li class="nav-item active">
					<a class="nav-link" href="bidding.jsp">其他服務 <span class="sr-only">(current)</span></a>
				</li>-->				
				<li class="nav-item">
					<a class="nav-link active" href="buyFlow.jsp">網站介紹 <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link active" href="packageSize.jsp">包裹大小限制 <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item ">
					<a class="nav-link active" href="expense.jsp">運費價格表 <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item ">
					<a class="nav-link active" href="Q&A.jsp">常見問題 <span class="sr-only">(current)</span></a>
				</li>

<!-- 				<li class="nav-item active"> -->
<!-- 					<a class="nav-link" href="#">客服中心 <span class="sr-only">(current)</span></a> -->
<!-- 				</li>				 -->
<!-- 				<li class="nav-item active"> -->
<!-- 					<a class="nav-link" th:href="@{/customerOrd}">會員中心 <span class="sr-only">(current)</span></a> -->
<!-- 				</li> -->
<!-- 				<li class="nav-item active"> -->
<!-- 					<a class="nav-link" th:href="@{/customerOrd}">我要下單 <span class="sr-only">(current)</span></a> -->
<!-- 				</li>				 -->
			</ul>
<!-- 			<form class="form-inline my-2 my-lg-0"> -->
<!-- 				<div class="input-group mb-0"> -->
<!-- 					<input type="text" class="form-control" placeholder="" aria-label="Recipient's username" aria-describedby="basic-addon2"> -->
<!-- 					<div class="input-group-append"> -->
<!-- 						<button class="btn btn-outline-secondary bg-white" type="button"><i class="fas fa-search"></i></button> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</form> -->
		</div>
	</nav>
