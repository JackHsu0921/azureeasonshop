<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>

	    <!-- Required meta tags -->   
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  	    
		
		<!-- Bootstrap CSS -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"/>
	
	    <!-- fontawesome -->
	    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css" integrity="sha384-v2Tw72dyUXeU3y4aM2Y0tBJQkGfplr39mxZqlTBDUZAb9BGoC40+rdFCG0m10lXk" crossorigin="anonymous"/>
	    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/regular.css" integrity="sha384-A/oR8MwZKeyJS+Y0tLZ16QIyje/AmPduwrvjeH6NLiLsp4cdE4uRJl8zobWXBm4u" crossorigin="anonymous"/>
	    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/brands.css" integrity="sha384-IiIL1/ODJBRTrDTFk/pW8j0DUI5/z9m1KYsTm/RjZTNV8RHLGZXkUDwgRRbbQ+Jh" crossorigin="anonymous"/>
	    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/fontawesome.css" integrity="sha384-q3jl8XQu1OpdLgGFvNRnPdj5VIlCvgsDQTQB6owSOHWlAurxul7f+JpUOVdAiJ5P" crossorigin="anonymous"/>
	
	    <!-- W3.CSS -->
	    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/> 
	    
	     <!--Css 自己的放最底-->
	    <link rel="stylesheet" type="text/css" href="css/indexcss.css"/>
	 	<link rel="icon" href="img/eason_ico.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="img/eason_ico.ico" type="image/x-icon" />
	  	<title>伊森日本網站代購</title>
