<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<%
	String twdjpyRate = "";
	twdjpyRate = (String)request.getServletContext().getAttribute("TWDJPYRate");
	if(twdjpyRate == null || twdjpyRate.length() == 0 ){
		twdjpyRate = "---";
	}
%>
<!DOCTYPE>
<html>
    <head>
    	<%@include file="includ/header.jsp" %>
  	</head>	
  	
  <body style="font-family:Microsoft JhengHei,monospace;" class="w3-light-gray">
  
	<!--navbar ==============================================================-->
	<%@include file="includ/navbar.jsp" %>
	
	<!-- ===========內容===================== -->
<!-- end-->
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<header class="w3-container w3-theme w3-center w3-bottombar w3-border-red w3-wide my-4" >
				  <h1 class="w3-xxlarge" style="text-shadow:1px 1px 0 #444">其他服務</h1>
				</header>
			</div>
			<div class="col-xs-12 col-sm-9">
				<div class="py-2">
					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card">
						<div class="w3-xlarge mb-3">
						  <b>有鑑於日本網站購買時遇到以下之情形：</b><br>
						  <font class="w3-text-red">1. 不接受海外信用卡。</font><br>
						  <font class="w3-text-red">2. 不知道如何下單購買。</font><br>
						  <font class="w3-text-red">3. 不可直接發送轉送業者地址。</font><br>
<!-- 						  <font>4. 本服務提供<font  class="w3-text-red">本服務提供代標後寄送你的日本集貨倉庫or民宿。</font></font><br> -->
<!-- 						  <font><a class="w3-text-blue" href="https://trackings.post.japanpost.jp/services/srv/search/input" target="_blank">日本郵便號碼追跡</a></font><br> -->
<!-- 						  <font><a class="w3-text-blue" href="http://k2k.sagawa-exp.co.jp/p/sagawa/web/okurijoinput.jsp" target="_blank">佐川急便號碼追跡</a></font><br> -->
					  	</div>
					</div>
				</div>
				<div class="py-2">
					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card">
						<div class="w3-xlarge mb-3">
						  <b>其他代收/代標/代集貨/代寄服務</b><br>
						  <font>1. 代收包裹(提供非倉庫或集貨業者地址)</font><br>
						  <small>費用說明：代收一件包裹250台幣，第二件開始每件200台幣</small><br>
						  <small>寄送說明：代收完成的包裹只能利用日本郵政寄回台灣或日本指定地址</small><br>
						  <font>2. 代標寄送你的日本集貨倉庫or民宿。</font><br>
						  <small>下標說明：由本站下標後指定至您的日本地址(追蹤碼包裹限定)</small><br>
						  <small>商品總價 = (稅後商品價格 + 日本國內運費、免運 0 運費) X 當日匯率 + 每件20台幣(最高只收100台幣)</small><br>
						  <font>3. 由本站代收或代集貨後寄到日本指定地址。</font><br>
						  <table class="w3-table w3-border w3-centered w3-white w3-border-all" align="center">
						  	<thead>						  		
						  		<tr>
						  			<th colspan="5" class="w3-amber">
						  				日本郵便
						  			</th>
						  		</tr>
						  		<tr class="w3-yellow">
						  			<th>
						  				3邊總和
						  			</th>
						  			<th>
						  				重量
						  			</th>
						  			<th>
						  				料金
						  			</th>
						  			<th>
						  				重量
						  			</th>
						  			<th>
						  				料金
						  			</th>
						  		</tr>
						  	</thead>
						  	<tr>
								<td>60cm以內</td>						  		
								<td>25kg以內</td>						  		
								<td>850日圓</td>						  		
								<td rowspan="7" align="center" style="vertical-align : middle;text-align:center;">25kg~30kg +500日圓</td>						  		
								<td>1350日圓</td>						  		
						  	</tr>
						  	<tr>
								<td>80cm以內</td>						  		
								<td>25kg以內</td>						  		
								<td>1080日圓</td>						  		
								<td>1580日圓</td>						  		
						  	</tr>
						  	<tr>
								<td>100cm以內</td>						  		
								<td>25kg以內</td>						  		
								<td>1310日圓</td>						  		
								<td>1810日圓</td>						  		
						  	</tr>
						  	<tr>
								<td>120cm以內</td>						  		
								<td>25kg以內</td>						  		
								<td>1560日圓</td>						  		
								<td>2060日圓</td>						  		
						  	</tr>
						  	<tr>
								<td>140cm以內</td>						  		
								<td>25kg以內</td>						  		
								<td>1800日圓</td>						  		
								<td>2300日圓</td>						  		
						  	</tr>
						  	<tr>
								<td>160cm以內</td>						  		
								<td>25kg以內</td>						  		
								<td>2020日圓</td>						  		
								<td>2520日圓</td>						  		
						  	</tr>
						  	<tr>
								<td>170cm以內</td>						  		
								<td>25kg以內</td>						  		
								<td>2370日圓</td>						  		
								<td>2870日圓</td>						  		
						  	</tr>
						  </table>
						  <br>
						  <a  href="https://line.me/R/ti/p/%40cfd3755c" target="_blank"><img class="w3-card" height="36" border="0" alt="加入好友" src="https://scdn.line-apps.com/n/line_add_friends/btn/zh-Hant.png"></a>
					  	</div>
					</div>
				</div>
				
			</div>
			<div class="col-xs-12 col-sm-3">
				<div class="col-xs-12 col-sm-12">
					<h3>本日匯率</h3>
				</div>
				<div class="col-xs-12 col-sm-12">
					<ul class="list-group w3-large">
					  <!-- <li   class="list-group-item">日幣 : <span id="jypRate"><%=twdjpyRate %></span></li>  -->
					  <li   class="list-group-item">日幣 : <span id="jypRate">0.276</span></li>
					</ul>
					<div class="text-center">
						<button onclick="document.getElementById('CountModal').style.display='block'" class="w3-btn w3-card w3-round w3-deep-purple w3-border text-center mt-3 ">點擊價格試算</button>	
					</div>
					
					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card">
						<div class="d-flex flex-column">
						  <div class="p-2">
						  		<h4><font class="w3-text-red">代標寄送</font>表單連結:</h4>
						  </div>
						  <div class="p-2">
						  		<a href="https://docs.google.com/forms/d/e/1FAIpQLSfD4uwkDPBR39vyoSkwiUss3cdc99F1xQoQSNWkj2mB4KvzaA/viewform" target="_blank" class="w3-btn w3-card w3-amber btn-lg btn-block" style="text-decoration:none;" ><i class="far fa-hand-pointer mr-3"></i>立即下單</a>
						  </div>
						  <div class="p-2">
						  		<a href="https://docs.google.com/forms/d/e/1FAIpQLSfi_n692p1Q5I-15KyyxI5T_OpR7fo8LREFKxs_ra_ahuPRBA/viewform" target="_blank" class="w3-btn w3-amber btn-lg btn-block" style="text-decoration:none;"><i class="fab fa-amazon-pay mr-3"></i>繳費</a>
						  </div>
						  <div class="p-2">
						  		<b class="w3-text-blue">繳費方式可選擇:</b>	
						  		<ul>
<!-- 								  <li >信用卡繳費(手續費2.75%)</li> -->
								  <li >ATM/WebATM(免手續費)</li>
								  <li >超商代碼繳款(手續費30台幣)</li>					  
								</ul>
						  </div>
<!-- 						  <div class="p-2"> -->
<!-- 						  		<a href="https://docs.google.com/forms/d/e/1FAIpQLSegaKnGZW0ulV8fHJf91lMfMku4hO_PpMuiMxncKv14S8uPBg/viewform" target="_blank" class="w3-btn w3-amber btn-lg btn-block" style="text-decoration:none;"><i class="fas fa-dolly-flatbed mr-3"></i>第二次繳費</a> -->
<!-- 						  </div> -->
						</div>
							
							
							
					</div>
										
				</div>	 			
			</div>
			
<!-- 			<div class="col-xs-12 col-sm-12"> -->
<!-- 				<div class="py-2"> -->
<!-- 					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card"> -->
<!-- 						<div class="w3-xlarge mb-3"> -->
<!-- 						  <b>簡單五步驟，讓您完成購買!</b><br> -->
<!-- 						  <font>1. 請先加入LINE好友之後，在網頁上選擇欲購買商品的日本網站。</font><br> -->
<!-- 						  <font>2. 商品確認後用填寫訂單表格，並確定網站、數量、番號、尺寸、顏色、日幣價格。</font><br> -->
<!-- 						  <font>3. 報價完成後，匯入款項，完成第一階段付款(下訂等廠商出貨)。</font><br> -->
<!-- 						  <font>4. 等待商品到日本集貨倉庫or民宿。</font><br> -->
<!-- 						  <font>5. 完成購買。</font><br> -->
<!-- 						  <a  href="https://line.me/R/ti/p/%40cfd3755c" target="_blank"><img class="w3-card" height="36" border="0" alt="加入好友" src="https://scdn.line-apps.com/n/line_add_friends/btn/zh-Hant.png"></a> -->
<!-- 					  	</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
			
			<div class="col-xs-12 col-sm-12 ">
					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card w3-large">
						    <div>			  
							  	<b class="w3-text-blue">付費:</b>		  
							  	<p>商品總價 = (稅後商品價格 + 日本國內運費、免運 0 運費) X 當日匯率 + 每件20台幣(最高只收100台幣)</p>
						    </div>
<!-- 						     <div >			   -->
<!-- 							  	<b class="w3-text-blue">第二階段付費:</b> -->
<!-- 			  					<p >寄送總價 = 可使用日本郵便/佐川急便寄送料金 X 當日匯率</p> -->
<!-- 						    </div>    -->
					</div>
			</div>
				
			
			<!-- 運費 -->
<!-- 			<div class="col-xs-12 col-sm-8 "> -->
<!-- 					<div class="py-2"> -->
<!-- 						<table class="w3-table-all w3-large w3-border-gray w3-hoverable"> -->
<!-- 						    <tr class="w3-blue"> -->
<!-- 						      <th colspan="5" class="w3-center">日本郵便</th> -->
<!-- 						    </tr> -->
<!-- 						    <tr> -->
<!-- 						      <th  class="w3-center">三邊總合</th> -->
<!-- 						      <th  class="w3-center">重量</th> -->
<!-- 						      <th  class="w3-center">料金</th> -->
<!-- 						      <th  class="w3-center">重量</th> -->
<!-- 						      <th  class="w3-center">料金</th> -->
<!-- 						    </tr> -->
<!-- 						    <tr> -->
<!-- 						      <td  class="w3-center">60cm以內</td> -->
<!-- 						      <td  class="w3-center">25kg以內</td> -->
<!-- 						      <td  class="w3-center">850日圓</td> -->
<!-- 						      <td rowspan="7"  class="w3-center w3-text-red" style="vertical-align : middle;text-align:center;">25kg~30kg +500日圓</td> -->
<!-- 						      <td  class="w3-center">1,350日圓</td> -->
<!-- 						    </tr> -->
<!-- 						     <tr> -->
<!-- 						      <td  class="w3-center">80cm以內</td> -->
<!-- 						      <td  class="w3-center">25kg以內</td> -->
<!-- 						      <td  class="w3-center">1,080日圓</td> -->
<!-- 						      <td  class="w3-center">1,580日圓</td> -->
<!-- 						    </tr>  -->
<!-- 						      <tr> -->
<!-- 						      <td  class="w3-center">100cm以內</td> -->
<!-- 						      <td  class="w3-center">25kg以內</td> -->
<!-- 						      <td  class="w3-center">1,310日圓</td> -->
<!-- 						      <td  class="w3-center">1,810日圓</td> -->
<!-- 						    </tr>  -->
<!-- 						      <tr> -->
<!-- 						      <td  class="w3-center">120cm以內</td> -->
<!-- 						      <td  class="w3-center">25kg以內</td> -->
<!-- 						      <td  class="w3-center">1,560日圓</td> -->
<!-- 						      <td  class="w3-center">2,060日圓</td> -->
<!-- 						    </tr>  -->
<!-- 						      <tr> -->
<!-- 						      <td  class="w3-center">140cm以內</td> -->
<!-- 						      <td  class="w3-center">25kg以內</td> -->
<!-- 						      <td  class="w3-center">1,800日圓</td> -->
<!-- 						      <td  class="w3-center">2,300日圓</td> -->
<!-- 						    </tr>  -->
<!-- 						      <tr> -->
<!-- 						      <td  class="w3-center">160cm以內</td> -->
<!-- 						      <td  class="w3-center">25kg以內</td> -->
<!-- 						      <td  class="w3-center">2,020日圓</td> -->
<!-- 						      <td  class="w3-center">2,520日圓</td> -->
<!-- 						    </tr>  -->
<!-- 						      <tr> -->
<!-- 						      <td  class="w3-center">170cm以內</td> -->
<!-- 						      <td  class="w3-center">25kg以內</td> -->
<!-- 						      <td  class="w3-center">2,370日圓</td> -->
<!-- 						      <td  class="w3-center">2,870日圓</td> -->
<!-- 						    </tr>  -->
						    
						   
<!-- 					    </table> -->
<!-- 					</div> -->
<!-- 				</div> -->
			
<!-- 			<div class="col-xs-12 col-sm-4 "> -->
<!-- 					<div class="py-2"> -->
<!-- 						<table class="w3-table-all w3-large w3-border-gray w3-hoverable"> -->
<!-- 						    <tr class="w3-blue"> -->
<!-- 						      <th colspan="5" class="w3-center">佐川急便</th> -->
<!-- 						    </tr> -->
<!-- 						    <tr> -->
<!-- 						      <th  class="w3-center">三邊總合</th> -->
<!-- 						      <th  class="w3-center">重量</th> -->
<!-- 						      <th  class="w3-center">料金</th> -->
<!-- 						    </tr> -->
<!-- 						    <tr> -->
<!-- 						      <td  class="w3-center">60cm以內</td> -->
<!-- 						      <td  class="w3-center">2kg以內</td> -->
<!-- 						      <td  class="w3-center">756日圓</td> -->
<!-- 						    </tr> -->
<!-- 						    <tr> -->
<!-- 						      <td  class="w3-center">80cm以內</td> -->
<!-- 						      <td  class="w3-center">5kg以內</td> -->
<!-- 						      <td  class="w3-center">1,026日圓</td> -->
<!-- 						    </tr> -->
<!-- 						    <tr> -->
<!-- 						      <td  class="w3-center">100cm以內</td> -->
<!-- 						      <td  class="w3-center">10kg以內</td> -->
<!-- 						      <td  class="w3-center">1,361日圓</td> -->
<!-- 						    </tr> -->
<!-- 						    <tr> -->
<!-- 						      <td  class="w3-center">140cm以內</td> -->
<!-- 						      <td  class="w3-center">20kg以內</td> -->
<!-- 						      <td  class="w3-center">1,815日圓</td> -->
<!-- 						    </tr> -->
<!-- 						    <tr> -->
<!-- 						      <td  class="w3-center">160cm以內</td> -->
<!-- 						      <td  class="w3-center">30kg以內</td> -->
<!-- 						      <td  class="w3-center">2,031日圓</td> -->
<!-- 						    </tr> -->
<!-- 						    <tr> -->
<!-- 						      <td  class="w3-center">170cm以內</td> -->
<!-- 						      <td rowspan="6"  class="w3-center w3-text-red" style="vertical-align : middle;text-align:center;" class="w3-center">50kg以內</td> -->
<!-- 						      <td  class="w3-center">2,376日圓</td> -->
<!-- 						    </tr> -->
<!-- 						    <tr> -->
<!-- 						      <td  class="w3-center">180cm以內</td> -->
<!-- 						      <td  class="w3-center">2,646日圓</td> -->
<!-- 						    </tr> -->
<!-- 						    <tr> -->
<!-- 						      <td  class="w3-center">200cm以內</td> -->
<!-- 						      <td  class="w3-center">3,186日圓</td> -->
<!-- 						    </tr> -->
<!-- 						    <tr> -->
<!-- 						      <td  class="w3-center">220cm以內</td> -->
<!-- 						      <td  class="w3-center">3,726日圓</td> -->
<!-- 						    </tr> -->
<!-- 						    <tr> -->
<!-- 						      <td  class="w3-center">240cm以內</td> -->
<!-- 						      <td  class="w3-center">4,806日圓</td> -->
<!-- 						    </tr> -->
<!-- 						    <tr> -->
<!-- 						      <td  class="w3-center">260cm以內</td> -->
<!-- 						      <td  class="w3-center">5,886日圓</td> -->
<!-- 						    </tr> -->
						   
						    
						   
<!-- 					    </table> -->
<!-- 					</div> -->
<!-- 				</div> -->
			
			
			
			
		</div>
	</div>
	
		<!-- 價格試算 Model	 -->
	<div id="CountModal" class="w3-modal">
		<div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
			<header class="w3-container w3-indigo">
				<span onclick="document.getElementById('CountModal').style.display='none'"
				class="w3-button w3-display-topright">&times;</span>
				<h2>價格試算</h2>
			</header>
			<div>			  
			  <div class="w3-pale-green px-3 py-2">			  
			  	<h4>價格:</h4>		  
			  	<p >商品總價 = (稅後商品價格 + 日本國內運費、免運 0 運費) X 當日匯率 + 每件20台幣(最高只收100台幣)</p>
<!-- 			  	<p >商品總報價(台幣) = (商品價格 + 商品價格10% + 日本國內運費 + 日本轉帳費) X 本日匯率</p> -->
			  </div>			  			  		    
			</div>

			<form class="w3-container">
				
				<label class="w3-text-blue"><b>商品價格總和(日幣)</b></label>
				<input class="w3-input w3-border" type="number" id="productPrice" onchange="firstPrice()" required>
				
				<label class="w3-text-blue"><b>一件商品20台幣(最高收取100台幣)</b></label>				
				<input class="w3-input w3-border" value="20" id="serviceFee" onchange="firstPrice()" type="text" required>				

				<label class="w3-text-blue"><b>日本國內運費總合(日幣)</b></label>
				<input class="w3-input w3-border" type="number" id="japFare" onchange="firstPrice()" value="0" required>


<!-- 				<label class="w3-text-blue"><b>日本轉帳費(日幣)</b></label> -->
<!-- 				<input class="w3-input w3-border" value="400" id="jpaTransfer" type="text" readonly> -->

				<label class="w3-text-blue"><b>當日即時匯率</b></label>
				<input class="w3-input w3-border" value="0.309" id="rate" type="text" readonly>													 
			</form> 

			<div style="display:none;" class="w3-container w3-border-top w3-padding-16 w3-light-grey mt-3">
				<p>試算結果(台幣): <span id="firstConut" class="w3-large w3-text-red"></span>
			</div>
<!-- 			<div class="w3-amber px-3 py-2"> -->
<!-- 			  	<h4>第二階段付費:</h4> -->
<!-- 			  	<p >國際運費報價(台幣) =  國際運費 X 本日匯率 + 國內運費</p> -->
<!-- 			  	<p >寄送總價 = 可使用日本郵便/佐川急便寄送料金 X 當日匯率</p> -->
<!-- 			</div> -->
<!-- 			<form class="w3-container"> -->
				
<!-- 				<label class="w3-text-blue"><b>日本郵便/佐川急便寄送料金(請參考下方運費表。單位:日幣)</b></label> -->
				<input type="hidden" class="w3-input w3-border" type="number" id="InternalPrice" onchange="secondPrice()" value="0" required>
								 
<!-- 				<label class="w3-text-blue"> -->
<!-- 					<b>日本郵便/佐川急便寄送料金 </b> -->
<!-- 				</label> -->
				<input type="hidden" class="w3-input w3-border" type="number" id="twFare" onchange="secondPrice()" value="0" required>

<!-- 			</form> -->
			<div class="w3-container w3-border-top w3-padding-16 w3-light-grey mt-3">
				<p style="display:none;">第二階段試算結果(台幣): <span id="secondCount" class="w3-large w3-text-red">0</span>
				<p class="w3-right">總計(台幣): <span id="totalCount" class="w3-xlarge w3-text-red"></span>
			</div>			
		</div>
	</div>
	
	
	


	
	<div class="container-fluid w3-red" style="height:8px;">
	</div>
	<div class="container-fluid w3-dark-gray ">
		<div class="row">
			<div class="container my-5">
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="d-flex flex-column">
						  <div class="p-2">
						  		<img class="w3-circle w3-card-4" src="img/eason_logo.jpg">
						  </div>
						  <div class="p-2">
						       <b class="w3-large ml-5 pl-4">Eason代購網</b>	
						  </div>
						</div>
					</div>
		   			<div class="col-xs-12 col-sm-8">
		   				<div class="d-flex justify-content-between">
		   					<div class="align-self-center w3-large">
			   					<b>歡迎加入Eason LINE@</b><br>
								<b>搜索ID  @cfd3755c 或請掃描 QRcode</b><br>					
								<b>2019 Copyright © All Rights Reserved</b>
								<br>
								<br>
								<a  href="https://line.me/R/ti/p/%40cfd3755c" target="_blank"><img class="w3-card" height="36" border="0" alt="加入好友" src="https://scdn.line-apps.com/n/line_add_friends/btn/zh-Hant.png"></a>
			   				</div>
			   				<div class="align-self-center">
			   					<img src="http://qr-official.line.me/L/5si8mM9h4l.png" class="w3-image">
			   				</div>
		   				</div>
		   			</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	
	
	
	
	
	
	
	<!-- ===========內容===================== -->
	<%@include file="includ/footer.jsp" %>
	<script src="js/indexjs0401.js"></script> 
</body>
</html>