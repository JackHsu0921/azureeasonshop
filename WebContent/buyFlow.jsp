<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE>
<html>
    <head>
    	<%@include file="includ/header.jsp" %>
  	</head>	
  	
  <body style="font-family:Microsoft JhengHei,monospace;" class="w3-light-gray"  >
  
	<!--navbar ==============================================================-->
	<%@include file="includ/navbar.jsp" %>
	
	<!-- ===========內容===================== -->
<!-- end-->
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<header class="w3-container w3-theme w3-center w3-bottombar w3-border-red w3-wide my-4" >
				  <h1 class="w3-xxlarge" style="text-shadow:1px 1px 0 #444">網站介紹</h1>
				</header>
			</div>
			<div class="col-xs-12 col-sm-12">
				<div class="py-2">
					<div class="w3-panel w3-bottombar w3-border-green w3-border w3-card">
						<div class="w3-xlarge mb-3">
							<h2 >簡介</h2>
							<div class="w3-large mb-3">
							  <font>大家好，我是伊森!!</font><br>
							  <font>我是長期移居日本關東地區的台灣人，會開始協助代購的主因是有鑑於，為了喜歡日貨的您，
							  		每次為了到日本採買，而花費不少的人力、時間、交通費與住宿。如果節省下以上的金錢、人力、時間的話，相信會更有效率。</font><br><br>
							  <font>我會幫您從日本直送台灣，安全、安心又快速，買一件也可以為您服務。
							  目前以日本網站的商品購入為主。
							  如有需要現場詢價或是幫您現場批貨時，另外討論收費方式。</font><br><br>
							  <font>另外提供代刷、代訂、代翻譯、代寄飯店等各種服務。</font><br><br>
							  <font>服務時間為每日：10:00~22:00</font><br><br>
							  <a  href="https://line.me/R/ti/p/%40cfd3755c" target="_blank"><img class="w3-card" height="36" border="0" alt="加入好友" src="https://scdn.line-apps.com/n/line_add_friends/btn/zh-Hant.png"></a>
						  	</div>
<!-- 						  <b>簡單五步驟，讓您完成購買!</b><br> -->
<!-- 						  <font>1. 請先加入LINE好友之後，在網頁上選擇欲購買商品的日本網站。</font><br> -->
<!-- 						  <font>2. 商品確認後用填寫訂單表格，並確定網站、數量、番號、尺寸、顏色、日幣價格。</font><br> -->
<!-- 						  <font>3. 報價完成後，匯入款項，完成第一階段付款(下訂等廠商出貨)。</font><br> -->
<!-- 						  <font>4. 國際運費報價，匯入款項，完成第二階段付款(等待商品到您手中)。</font><br> -->
<!-- 						  <font>5. 完成購買。</font><br> -->
					  	</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>


	
	<div class="container-fluid w3-red" style="height:8px;">
	</div>
	<div class="container-fluid w3-dark-gray ">
		<div class="row">
			<div class="container my-5">
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="d-flex flex-column">
						  <div class="p-2">
						  		<img class="w3-circle w3-card-4" src="img/eason_logo.jpg">
						  </div>
						  <div class="p-2">
						       <b class="w3-large ml-5 pl-4">Eason代購網</b>	
						  </div>
						</div>
					</div>
		   			<div class="col-xs-12 col-sm-8">
		   				<div class="d-flex justify-content-between">
		   					<div class="align-self-center w3-large">
			   					<b>歡迎加入Eason LINE@</b><br>
								<b>搜索ID  @cfd3755c 或請掃描 QRcode</b><br>					
								<b>2019 Copyright © All Rights Reserved</b>
								<br>
								<br>
								<a  href="https://line.me/R/ti/p/%40cfd3755c" target="_blank"><img class="w3-card" height="36" border="0" alt="加入好友" src="https://scdn.line-apps.com/n/line_add_friends/btn/zh-Hant.png"></a>
			   				</div>
			   				<div class="align-self-center">
			   					<img src="http://qr-official.line.me/L/5si8mM9h4l.png" class="w3-image">
			   				</div>
		   				</div>
		   			</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	
	
	
	
	
	
	
	<!-- ===========內容===================== -->
	<%@include file="includ/footer.jsp" %>
</body>
</html>