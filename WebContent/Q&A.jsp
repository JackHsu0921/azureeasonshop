<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<!DOCTYPE>
<html>
    <head>
    	<%@include file="includ/header.jsp" %>
  	</head>	
  	
  <body style="font-family:Microsoft JhengHei,monospace;" >
  
	<!--navbar ==============================================================-->
	<%@include file="includ/navbar.jsp" %>
	
	<!-- ===========內容===================== -->

	<nav class="w3-sidebar w3-bar-block w3-collapse w3-animate-left w3-card w3-light-gray w3-large" style="z-index:3;width:250px;" id="mySidebar">
        <a class="w3-bar-item w3-button w3-hide-large w3-large" href="javascript:void(0)" onclick="w3_close()">Close <i class="fa fa-remove"></i></a>
        <a class="w3-bar-item w3-button w3-blue" href="#">列表</a>
        <a class="w3-bar-item w3-button" href="#q1">有什麼東西不能配送(違禁品)?</a>
        <a class="w3-bar-item w3-button" href="#q2">日本國內運送到台灣天數(不含假日)</a>
        <a class="w3-bar-item w3-button" href="#q3">商品重量大小限制</a>
        <a class="w3-bar-item w3-button" href="#q4">損壞與退換貨機制</a>
        <a class="w3-bar-item w3-button" href="#q5">是否提供併貨發送</a>
        <a class="w3-bar-item w3-button" href="#q6">超過多少會被海關課取關稅?</a>
        <a class="w3-bar-item w3-button" href="#q7">本站收到商品時會檢查後寄出嗎?</a>
        <a class="w3-bar-item w3-button" href="#q8">可以接受貨到付款嗎</a>
        <a class="w3-bar-item w3-button" href="#q9">我的商品現在配送到哪了?</a>
        <a class="w3-bar-item w3-button" href="#q10">可以取消訂單嗎?</a>
    </nav>



    <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" id="myOverlay"></div>

    <div class="w3-main" style="margin-left:250px;">

<!--      <div id="myTop" class="w3-container w3-top w3-theme w3-large"> -->
<!--          <p><i class="fa fa-bars w3-button w3-teal w3-hide-large w3-xlarge" onclick="w3_open()"></i>  -->
<!--          <span id="myIntro" class="w3-hide">W3.CSS: Introduction</span></p>  -->
<!--      </div>  -->

<header class="w3-container w3-theme w3-bottombar w3-border-red w3-wide px-3 mx-3" >
  <h1 class="w3-xxlarge" style="text-shadow:1px 1px 0 #444">Eason 日本代購Q&A</h1>
</header>

<div class="w3-container" style="padding:32px">
<div class="mb-3"><a name="q1" >&nbsp</a></div>

<div class="question1 w3-white px-1 py-1 w3-border w3-leftbar w3-border-blue ">
<h2>有什麼東西不能配送(違禁品)?</h2>
<p class="w3-xlarge">細項可點擊連結參考<a href="https://www.post.japanpost.jp/int/use/restriction/upc_cn.html" target="_blank"><b class="w3-text-red">萬國郵政公約所規定的禁制品</b></a></p>
<div class="container w3-large">
    <div class="row">
        
 

<div class="col-xs-12 col-sm-3">
	<div class="large">
	    <h4>火藥類</h4>
	    <ul>
	        <li>煙火</li>
	        <li>拉炮</li>
	        <li>彈藥</li>
	        <li>點火裝置</li>
	        <li>炸藥</li>
	        <li>保險絲</li>
	    </ul>
    </div>
</div>
<div class="col-xs-12 col-sm-3">
    <h4>高壓氣體</h4>
    <ul>
        <li>高壓噴霧罐</li>
        <li>火機補充瓦斯</li>
        <li>潛水用氧氣瓶</li>
        <li>露營用瓦斯爐</li>
        <li>罐裝瓦斯</li>
        <li>滅火器/li>
        <li>高壓除塵空氣罐</li>
        <li>攜帶式氧氣罐</li>
        <li>氦氣瓶</li>
        <li>氯氟烴</li>
        <li>瓦斯暖爐</li>
        <li>汽車懸吊系統</li>
    </ul>
</div>
<div class="col-xs-12 col-sm-3">
    <h4>易燃液體</h4>
    <ul>
        <li>打火機油</li>
        <li>Zippo打火機</li>
        <li>油漆</li>
        <li>酒精</li>
        <li>塗料</li>
        <li>稀釋劑</li>
        <li>亮光漆</li>
        <li>香水</li>
        <li>指甲油</li>
        <li>精油</li>
        <li>生髮水</li>
        <li>接著劑</li>
        <li>Tempo Drop</li>
    </ul>
</div>
<div class="col-xs-12 col-sm-3">
    <h4>易燃物質</h4>
    <ul>
        <li>火柴</li>
        <li>炭</li>
        <li>燈油</li>
        <li>各式暖爐</li>
    </ul>
</div>
<div class="col-xs-12 col-sm-3">
    <h4>氧化性物質</h4>
    <ul>
        <li>個人用小型氧氣製造機</li>
        <li>漂白劑</li>
        <li>過氧化氫</li>
        <li>染髮劑</li>
    </ul>
</div>
<div class="col-xs-12 col-sm-3">
    <h4>毒性物質</h4>
    <ul>
        <li>水蒸式殺蟲劑</li>
        <li>農藥</li>
        <li>氯仿</li>
    </ul>
</div>
<div class="col-xs-12 col-sm-3">
    <h4>放射性物質</h4>
    <ul>
        <li>鈾</li>
        <li>鈽</li>
        <li>銫</li>
        <li>釷</li>
        <li>核廢料</li>
    </ul>
</div>
<div class="col-xs-12 col-sm-3">
    <h4>腐蝕性物質</h4>
    <ul>
        <li>液體電池</li>
        <li>水銀</li>
    </ul>
</div>
<div class="col-xs-12 col-sm-3">
    <h4>其他物品</h4>
    <ul>
        <li>第一類藥品</li>
        <li>磁鐵</li>
        <li>引擎</li>
        <li>乾冰</li>
        <li>冷媒</li>
        <li>石棉</li>
        <li>毒品</li>
        <li>精神藥品</li>
        <li>消臭劑</li>
        <li>彩券</li>
        <li>空氣槍</li>
        <li>玩具槍</li>
        <li>模型槍</li>
    </ul>
</div>
<div class="col-xs-12 col-sm-3">
    <h4>其他物品</h4>
    <ul>
        <li>模造刀（美術刀）</li>
        <li>信用卡</li>
        <li>有價證券</li>
        <li>票券（娛樂活動門票、交通車票等等）</li>
        <li>機票</li>
        <li>家畜和寵物飼料</li>
        <li>植物（土・盆栽・種子）</li>
        <li>肥料</li>
        <li>生鮮食品</li>
        <li>動物</li>
        <li>稀有動物皮毛</li>
        <li><a href="https://www.post.japanpost.jp/int/use/restriction/upc_cn.html" target="_blank">華盛頓條約內所限制的物品</a></li>
        <li>貴金屬（金・銀・白金）</li>
    </ul>
</div>
<div class="col-xs-12 col-sm-3">
    <h4>其他物品</h4>
    <ul>
        <li>黃金及黃金製品</li>
        <li>鑽石等寶石類（不論是否加工）</li>
        <li>珍珠</li>
        <li>偽造貨幣</li>
        <li>郵票或印花</li>
        <li>個人文件</li>
        <li>國家級重要財產</li>
        <li>重要文化遺產及美術品</li>
        <li>盜版拷貝光碟</li>
    </ul>
</div>

   </div> <!-- row end  -->
</div> <!-- container end  -->
</div>

<div class="mb-3"><a name="q2">&nbsp</a></div>
<div class="question1 px-1 py-1 w3-border w3-leftbar w3-border-blue">
	<h2>日本國內運送到台灣的天數?</h2>
	<ul class="w3-large">
		<li>
			<table class="table table-bordered w3-striped w3-hoverable">
				<tr class="w3-yellow">
					<th>運送方式</th>
					<th>發貨→抵台工作天</th>
				</tr>
				<tr>
					<td>日本郵局EMS</td>
					<td>3~4天</td>
				</tr>
				<tr>
					<td>日本郵局空運小包</td>
					<td>6~8天</td>
				</tr>
				<tr>
					<td>日本郵局空運</td>
					<td>6~8天</td>
				</tr>
				<tr>
					<td>日本郵局海運</td>
					<td>1~2個月</td>
				</tr>
				<tr>
					<td>集貨空運 (包關稅)</td>
					<td>2週</td>
				</tr>
				<tr>
					<td>集貨海運 (包關稅)</td>
					<td>1~2個月</td>
				</tr>
			</table>
		</li>
	    <li class="w3-text-red">如遇到日本或台灣連假則往後延</li>
	</ul>
</div>

<div class="mb-3"><a name="q3" >&nbsp</a></div> 
<div class="question1 w3-border px-1 py-1 w3-leftbar w3-border-blue">
	<h2>商品重量大小限制?</h2>
	<ul class="w3-large">
	    <li>以<font class="w3-text-red">集貨空運</font>為例，其他包裹<a href="packageSize.jsp" class="w3-text-blue" target="_blank">請參考</a>。</li>
	    <li>重量限制為20KG以內</li>
	    <li>包裹長寬高其中一邊不可超過 80CM、或長寬高三邊總和不可超過 150CM</li>
	    <li>
	    	<img src="img/packageSizeNew.png" class="w3-image">
	    </li>
	    <li><font class="w3-text-red">體積重</font>與<font class="w3-text-red">實重</font>比例不可超過<font class="w3-text-red">5倍</font></li>
	    <li>體積重計算方法：(長cm X 闊cm X 高cm) / 6000 X 2.2046</li>
	    <li>例如：	體積重 (80cm X 40cm X 30cm) / 6000 X 2.2046 = 35.3KG<br>
	    	<font class="w3-text-red">實重</font>就要<font class="w3-text-red">大於</font> 7.05KG (35.3 / 5 = 7.05kg)</li>
	    <li>如超出以上其中一項限制，有機會需要付出額外費用</li>
	    <li>常見超出比例的物品如下：<br>
	    大型玩具、公仔、布偶、嬰兒車、腳踏車、寢具類商品等….</li>
	</ul>
</div>

<div class="mb-3"><a name="q4" >&nbsp</a></div>
<div class="question1 w3-border px-1 py-1 w3-leftbar w3-border-blue">
	<h2>損壞與退換貨機制?</h2>
	<ul class="w3-large">
	    <li>因本站只負責代標寄送，不接受退換貨。</li>
	</ul>
</div>

<div class="mb-3"><a name="q5" >&nbsp</a></div>
<div class="question1 w3-border px-1 py-1 w3-leftbar w3-border-blue">
	<h2>是否提供併貨發送?</h2>
	<ul class="w3-large">
		<li>可以合併多件包裹後，幫您寄出，不收服務費。</li>
	</ul>
</div>

<div class="mb-3"><a name="q6" >&nbsp</a></div>
<div class="question1 w3-border px-1 py-1 w3-leftbar w3-border-blue">
	<h2 >超過多少會被海關課取關稅?</h2>
	<ul class="w3-large">
	    <li>利用日本郵局EMS/空運/海運/空運小包者，台灣關稅規定總價超過2000台幣的商品，有可能會被海關將課取關稅。(買家下單前請先了解)</li>
	    <li>利用集貨空運 (包關稅)者， 由我方幫您支付</li>
	    <li>利用集貨海運 (包關稅)者， 由我方幫您支付</li>
	</ul>
</div>

<div class="mb-3"><a name="q7" >&nbsp</a></div>
<div class="question1 w3-border px-1 py-1 w3-leftbar w3-border-blue">
	<h2>本站收到商品時會檢查後寄出嗎?</h2>
	<ul class="w3-large">
	    <li>為保護您得隱私，您可以選擇檢查或不檢查選項。</li>
	    <li>如選擇檢查時本站會檢查商品數量是否無誤，但無法檢查商品是否有瑕疵等細部問題。</li>
	    <li>為衛生因素起見，本站不將衣服或商品從原封袋中取出</li>
	</ul>
</div>

<div class="mb-3"><a name="q8" >&nbsp</a></div>
<div class="question1 w3-border px-1 py-1 w3-leftbar w3-border-blue">
	<h2>可以超商取貨or貨到付款嗎?</h2>
	<ul class="w3-large">
	    <li>無超商取貨跟貨到付款服務。</li>
	    <li>日本郵政寄送 EMS 空運小包 空運 海運 直送到家(無超取服務) 免台灣運費。</li>
	    <li>集貨空運寄送直送到家免台灣運費。。</li>
	    <li>集貨海運運寄送直送到家免台灣運費。</li>
	</ul>
</div>

<div class="mb-3"><a name="q9" >&nbsp</a></div>
<div class="question1 w3-border px-1 py-1 w3-leftbar w3-border-blue">
	<h2>我的商品現在配送到哪了?</h2>
	<ul class="w3-large">
	    <li>使用EMS 時，會提供你追跡號碼(其餘運送方式則無)</li>
	    <li class="w3-text-red"><a href="https://www.post.japanpost.jp/int/ems/delivery/index.html">EMS 追跡</a></li>
	</ul>
</div>

<div class="mb-3"><a name="q10" >&nbsp</a></div>
<div class="question1 w3-border px-1 py-1 w3-leftbar w3-border-blue">
	<h2>可以取消訂單嗎?</h2>
	<ul class="w3-large">
	    <li>匯款完成後，不接受取消訂單。</li>
	</ul>
</div>

</div>
    
    
    
    <div class="container-fluid w3-red" style="height:8px;">
	</div>
	<div class="container-fluid w3-dark-gray ">
		<div class="row">
			<div class="container my-5">
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="d-flex flex-column">
						  <div class="p-2">
						  		<img class="w3-circle w3-card-4" src="img/eason_logo.jpg">
						  </div>
						  <div class="p-2">
						       <b class="w3-large ml-5 pl-4">Eason代購網</b>	
						  </div>
						</div>
					</div>
		   			<div class="col-xs-12 col-sm-8">
		   				<div class="d-flex justify-content-between">
		   					<div class="align-self-center w3-large">
			   					<b>歡迎加入Eason LINE@</b><br>
								<b>搜索ID  @cfd3755c 或請掃描 QRcode</b><br>					
								<b>2019 Copyright © All Rights Reserved</b>
								<br>
								<br>
								<a  href="https://line.me/R/ti/p/%40cfd3755c" target="_blank"><img class="w3-card" height="36" border="0" alt="加入好友" src="https://scdn.line-apps.com/n/line_add_friends/btn/zh-Hant.png"></a>
			   				</div>
			   				<div class="align-self-center">
			   					<img src="http://qr-official.line.me/L/5si8mM9h4l.png" class="w3-image">
			   				</div>
		   				</div>
		   			</div>
				</div>
			</div>
		</div>
	</div> 
</div> <!-- end-->




	
	
	
	
	
	
	
	
	
	
	
	
	<!-- ===========內容===================== -->
	<%@include file="includ/footer.jsp" %>
</body>
</html>